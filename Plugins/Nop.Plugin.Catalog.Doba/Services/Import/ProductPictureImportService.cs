﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.Doba.Services.Import
{
	public class ProductPictureImportService : IProductImageImportService
	{
		readonly IProductService _productPictureService;
		readonly IImageImportService _imageImportService;

		object _pictureServiceLock = new object();

		public ProductPictureImportService(IImageImportService imageImportService, IProductService productPictureService)
		{
			_productPictureService = productPictureService;
			_imageImportService = imageImportService;
		}

		public async Task ImportImages(int productId, IEnumerable<string> productImagery)
		{
			IEnumerable<Picture> pictures = await _imageImportService.ImportPictures(productImagery);
			IEnumerable<ProductPicture> productPictures = pictures.Select((picture, index) =>
			{
				return new ProductPicture {
					ProductId = productId,
					Picture = picture,
					DisplayOrder = index
				};
			}).ToArray();

			foreach (ProductPicture productPicture in productPictures)
			{
				lock (_pictureServiceLock)
				{
					_productPictureService.InsertProductPicture(productPicture);
				}
			}
		}
	}
}
