﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class SourceInvalidValueModel : BaseEntityModel
	{
		public int SourceId { get; set; }
		public string Value { get; set; }
		public bool IsRegex { get; set; }
	}
}