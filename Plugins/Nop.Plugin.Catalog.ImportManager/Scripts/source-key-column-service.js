﻿var sourceKeyColumnService = function (collectionSubscription, qsService) {
	var restService = new baseService('source-key-column', collectionSubscription);
	return $.extend(restService, {
		getSourceKeysForCollection: function (collectionId) {
			var qString = qsService.by('sourceClassMapping.importsource.collectionId', filterType.EQUAL, collectionId).build();
			return this.get('?filter=' + qString, 'GETBYCOLLECTION');
		},
		getByMappingId: function (mappingId) {
			var qString = qsService.by('sourceClassMappingId', filterType.EQUAL, mappingId).build();
			return this.get('?filter=' + qString, 'GETBYMAPPING');
		},
		getParentKeys: function (sourceId, mappingId, entityType) {
			var qString = qsService.by('SourceClassMapping.SourceId', filterType.EQUAL, sourceId)
				.and('SourceClassMappingId', filterType.NOTEQUAL, mappingId)
				.and('SourceClassMapping.EntityType', filterType.NOTEQUAL, entityType).build();
			return this.get('?filter=' + qString, 'GETDEPEDENTYKEYS');
		}
	});
};