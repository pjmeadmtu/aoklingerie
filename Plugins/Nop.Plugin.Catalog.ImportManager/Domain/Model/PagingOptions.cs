﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class PagingOptions
	{
		public int? Page { get; set; }
		public int? PageSize { get; set; }
		public string Filter { get; set; }
		public string OrderBy { get; set; }

		public bool HasPaging
		{
			get
			{
				return Page.HasValue && PageSize.HasValue && Page.Value > 0 && PageSize.Value > 0;
			}
		}
	}
}