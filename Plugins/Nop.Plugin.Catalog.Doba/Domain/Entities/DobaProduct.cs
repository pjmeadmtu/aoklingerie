﻿using System;
using Nop.Core;

namespace Nop.Plugin.Catalog.Doba.Domain.Entities
{
    public class DobaProduct : BaseEntity
    {
		public string ItemId { get; set; }
		public string Sku { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public decimal ShipCost { get; set; }
		public decimal DropshipFee { get; set; }
		public int QuantityAvailable { get; set; }
		public int ProductId { get; set; }
		public int? InternalProductId { get; set; }
		public decimal ShipWeight { get; set; }
		public decimal MSRP { get; set; }
		public DateTime? EstAvail { get; set; }
		public string ImageFile { get; set; }
		public string AdditionalImages { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime? LastUpdated { get; set; }
		public decimal PrepayPrice { get; set; }
		public string SupplierName { get; set; }
    }
}