﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class ImportSourceService : BaseDataService<ImportSource, ImportSourceModel>, IImportSourceService
	{
		readonly IImportSourceRepository _importSourceRepository;
		readonly IImportSourceColumnRepository _importSourceColumnRepository;
		readonly IMapper _mapper;

		public ImportSourceService(
			IImportSourceRepository repository,
			IQueryStringExpressionService<ImportSource> qsExpressionService,
			IMapper mapper,
			IImportSourceColumnRepository importSourceColumnRepository)
			: base(repository, qsExpressionService, mapper)
		{
			_importSourceRepository = repository;
			_importSourceColumnRepository = importSourceColumnRepository;
			_mapper = mapper;
		}

		public IEnumerable<ImportSourceModel> GetForCollection(int collectionId)
		{
			IEnumerable<ImportSource> sources = _importSourceRepository.Where(p => p.ImportCollectionId == collectionId).ToArray();
			return sources.Select(p => _mapper.Map<ImportSourceModel>(p)).ToArray();
		}

		public Stream GetSourceStream(int sourceId)
		{
			ImportSource source = _importSourceRepository.GetById(sourceId);
			if (source.Source == null) return null;

			if (source.SourceType == SourceType.Url)
			{
				throw new NotImplementedException();
			}

			return new MemoryStream(source.Source);
		}

		public async Task<ImportSourceModel> InsertAsync(ImportSourceModel model)
		{
			ImportSource entity = _mapper.Map<ImportSourceModel, ImportSource>(model);
			entity.Source = await GetFileUploadBytes(model.FileUpload);
			_importSourceRepository.Insert(entity);
			CreateColumnNames(entity.Source, entity.Id, model.SourceType);

			return _mapper.Map<ImportSource, ImportSourceModel>(entity);
		}

		public async Task<ImportSourceModel> UpdateAsync(ImportSourceModel model)
		{
			ImportSource importSource = _importSourceRepository.GetById(model.Id);
			byte[] fileBytes = await GetFileUploadBytes(model.FileUpload);

			importSource = _mapper.Map(model, importSource);
			if (fileBytes != null) importSource.Source = fileBytes;

			_importSourceRepository.Update(importSource);

			return _mapper.Map<ImportSource, ImportSourceModel>(importSource);
		}

		public async Task<ImportSourceModel> SaveAsync(ImportSourceModel importSourceModel)
		{
			return importSourceModel.Id.HasValue
				? await UpdateAsync(importSourceModel)
				: await InsertAsync(importSourceModel);
		}

		void CreateColumnNames(byte[] bytes, int importSourceId, SourceType sourceType)
		{
			using (var memStream = new MemoryStream(bytes))
			using (IExcelDataReader dataReader = (sourceType == SourceType.CSV ? ExcelReaderFactory.CreateCsvReader(memStream) : ExcelReaderFactory.CreateReader(memStream)))
			{
				IEnumerable<ImportSourceColumn> sourceColumns = dataReader.GetHeaderValues()
					.Select((p, index) => new ImportSourceColumn
					{
						SourceId = importSourceId,
						Index = index,
						Name = string.IsNullOrWhiteSpace(p) ? index.ToString() : p.ToString()
					});

				sourceColumns.Iterate(_importSourceColumnRepository.Insert);
			}
		}

		async Task<byte[]> GetFileUploadBytes(IFormFile fileUpload)
		{
			if (fileUpload == null) return null;

			using (var memStream = new MemoryStream())
			{
				await fileUpload.CopyToAsync(memStream);
				return memStream.ToArray();
			}
		}
	}
}