﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.Doba.Domain.Entities;

namespace Nop.Plugin.Catalog.Doba.Infrastructure.Database.Mapping
{
    public class DobaProductMap : NopEntityTypeConfiguration<DobaProduct>
	{
		public DobaProductMap()
		{
			ToTable("DobaProduct");
			HasKey(p => p.Id);
			Property(p => p.Title).HasMaxLength(400).IsRequired();
			Property(p => p.Sku).HasMaxLength(400).IsRequired();
			Property(p => p.ImageFile).HasMaxLength(400).IsRequired();
			Property(p => p.DropshipFee).IsRequired();
			Property(p => p.ItemId).IsRequired().HasMaxLength(50);
			Property(p => p.Price).IsRequired();
			Property(p => p.PrepayPrice).IsRequired();
			Property(p => p.QuantityAvailable).IsRequired();
			Property(p => p.ShipCost).IsRequired();
			Property(p => p.ProductId).IsOptional();
			Property(p => p.AdditionalImages).HasMaxLength(4000).IsOptional();
			Property(p => p.InternalProductId).IsOptional();
			Property(p => p.EstAvail).IsOptional();
			Property(p => p.ShipWeight).IsOptional();
			Property(p => p.DateCreated).IsRequired();
			Property(p => p.LastUpdated).IsOptional();
			Property(p => p.SupplierName).HasMaxLength(200).IsOptional();
		}
    }
}