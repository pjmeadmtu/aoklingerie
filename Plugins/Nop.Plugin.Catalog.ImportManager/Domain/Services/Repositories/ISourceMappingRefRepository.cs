﻿using System.Collections.Generic;

using Nop.Core.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories
{
	public interface ISourceMappingRefRepository : IRepository<SourceMappingRef>
	{
		IEnumerable<SourceMappingRef> GetForSourceAndEntityType(int sourceId, ImportEntityType entityType);
	}
}