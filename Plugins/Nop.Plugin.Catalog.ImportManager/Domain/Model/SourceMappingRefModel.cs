﻿using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class SourceMappingRefModel : BaseEntityModel
	{
		public int SourceId { get; set; }
		public int EntityId { get; set; }
		public string SourceIdValue { get; set; }
		public ImportEntityType EntityType { get; set; }
	}
}