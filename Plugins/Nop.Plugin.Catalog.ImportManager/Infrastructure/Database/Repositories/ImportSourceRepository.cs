﻿using Nop.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories
{
	public class ImportSourceRepository : EfRepository<ImportSource>, IImportSourceRepository
	{
		public ImportSourceRepository(IImportManagerContext context) : base(context) { }
	}
}