﻿using System;

namespace Nop.Plugin.Catalog.Doba.Helpers
{
	internal static class ConversionHelper
    {
		public static T? Parse<T>(string value, Func<string, T> parse)
			where T : struct
		{
			if (!string.IsNullOrEmpty(value))
				return (T?)parse(value);

			return null;
		}
    }
}