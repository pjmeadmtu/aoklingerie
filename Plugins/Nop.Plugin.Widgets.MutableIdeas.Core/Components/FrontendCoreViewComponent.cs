﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.MutableIdeas.Core.Components
{
	[ViewComponent(Name = Consts.FRONTENDCORE)]
	public class FrontendCoreViewComponent : NopViewComponent
	{
		public IViewComponentResult Invoke()
		{
			IEnumerable<string> scriptPaths = GetScriptPaths();
			return View($"{Consts.COMPONENTS_PATH}/PublicInfo.cshtml", scriptPaths);
		}

		IEnumerable<string> GetScriptPaths()
		{
			string path = Path.Combine(Environment.CurrentDirectory, Consts.SCRIPTS_PATH);
			DirectoryInfo dirInfo = new DirectoryInfo(path);
			return dirInfo.GetFiles("*.js").Select(p => string.Join("/", "~", Consts.SCRIPTS_PATH, p.Name)).ToArray();
		}
	}
}