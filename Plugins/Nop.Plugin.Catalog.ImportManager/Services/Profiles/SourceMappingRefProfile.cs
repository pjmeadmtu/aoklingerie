﻿using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class SourceMappingRefProfile : Profile
	{
		public SourceMappingRefProfile()
		{
			CreateMap<SourceMappingRef, SourceMappingRefModel>();
			CreateMap<SourceMappingRefModel, SourceMappingRef>()
				.Map(p => p.Id.HasValue ? p.Id.Value : -1, p => p.Id)
				.Ignore(p => p.ImportSource);
		}
	}
}