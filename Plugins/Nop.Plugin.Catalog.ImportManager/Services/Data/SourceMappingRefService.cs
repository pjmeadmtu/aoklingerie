﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class SourceMappingRefService : BaseDataService<SourceMappingRef, SourceMappingRefModel>, ISourceMappingRefService
	{
		readonly ISourceMappingRefRepository _sourceMappingRefRepository;

		public SourceMappingRefService(ISourceMappingRefRepository repository,
			IQueryStringExpressionService<SourceMappingRef> querystringExpressionService, IMapper mapper)
			: base(repository, querystringExpressionService, mapper)
		{
			_sourceMappingRefRepository = repository;
		}

		public IEnumerable<SourceMappingRefModel> GetBySourceAndEntity(int sourceId, ImportEntityType entityType)
		{
			IEnumerable<SourceMappingRef> sourceMappingRefs = _sourceMappingRefRepository.GetForSourceAndEntityType(sourceId, entityType)
				.ToArray();
			return Map(() => sourceMappingRefs);
		}
	}
}