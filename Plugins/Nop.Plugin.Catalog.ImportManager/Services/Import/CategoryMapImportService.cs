﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class CategoryMapImportService : BaseImportService<ProductCategory>, ICategoryMapImportService
	{
		public CategoryMapImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			ICategoryService categoryService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.CategoryMap,
				  categoryService.InsertProductCategory,
				  categoryService.UpdateProductCategory) { }
	}
}