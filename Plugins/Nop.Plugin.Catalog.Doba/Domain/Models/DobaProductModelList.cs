﻿using System.Xml.Serialization;

namespace Nop.Plugin.Catalog.Doba.Domain.Models
{
	[XmlRoot("itemlist")]
    public class DobaProductModelList
    {
		[XmlElement("item")]
		public DobaProductImportModel[] Products { get; set; }
    }
}