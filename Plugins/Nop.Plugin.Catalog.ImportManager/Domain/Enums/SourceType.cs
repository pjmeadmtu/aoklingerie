﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Enums
{
	public enum SourceType
	{
		Url = 0,
		CSV = 1,
		Tab = 2,
		Excel = 3,
		Json = 4
	}
}
