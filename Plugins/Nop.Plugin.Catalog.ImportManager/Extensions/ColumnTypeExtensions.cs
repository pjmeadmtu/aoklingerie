﻿using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class ColumnTypeExtensions
	{
		public static bool Includes(this ColumnType columnValues, ColumnType value)
		{
			return (value & columnValues) == value;
		}
	}
}