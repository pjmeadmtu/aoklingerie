﻿using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Import
{
	public interface IEntityImportService
	{
		void Import(SourceClassMappingModel sourceClassMappingModel);
	}
}