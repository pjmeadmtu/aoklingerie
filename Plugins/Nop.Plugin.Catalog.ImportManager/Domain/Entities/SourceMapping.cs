﻿using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class SourceMapping : BaseEntity
	{
		public int SourceClassMappingId { get; set; }
		public ColumnType ColumnType { get; set; }
		public int? ReferenceClassMappingId { get; set; }
		public int? SourceColumnId { get; set; }

		public string DefaultValue { get; set; }
		public string DestinationPropertyName { get; set; }
		public bool Overwrite { get; set; }

		public virtual SourceClassMapping SourceClassMapping { get; set; }
		public virtual SourceClassMapping ReferenceClassMapping { get; set; }
		public virtual ImportSourceColumn ImportSourceColumn { get; set; }	
	}
}