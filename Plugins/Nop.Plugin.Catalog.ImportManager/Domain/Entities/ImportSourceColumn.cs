﻿using Nop.Core;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class ImportSourceColumn : BaseEntity
	{
		public int SourceId { get; set; }
		public string Name { get; set; }
		public int Index { get; set; }

		public virtual ImportSource ImportSource { get; set; }
		public virtual ICollection<SourceKeyColumn> SourceColumnKeys { get; set; }
		public virtual ICollection<SourceMapping> SourceMappings { get; set; }
	}
}