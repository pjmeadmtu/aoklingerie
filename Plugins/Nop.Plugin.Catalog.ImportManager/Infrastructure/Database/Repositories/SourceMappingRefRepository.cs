﻿using System.Collections.Generic;
using Nop.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories
{
	public class SourceMappingRefRepository : EfRepository<SourceMappingRef>, ISourceMappingRefRepository
	{
		public SourceMappingRefRepository(IImportManagerContext context) : base(context) { }

		public IEnumerable<SourceMappingRef> GetForSourceAndEntityType(int sourceId, ImportEntityType entityType)
		{
			return Where(p => p.SourceId == sourceId && p.EntityType == entityType);
		}
	}
}
