﻿using System;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Enums
{
	[Flags]
	public enum ColumnType
	{
		RowValue = 1,
		RowValueIdentifier = 2,
		ColumnNameAsIdentifier = 4,
		DefaultToValue = 8,
		ReferenceValue = 16,
		CompositeKeyReferenceValue = 32,
		CompositeKeyValue = 64,
		ColumnNameAsValue = 128,
		IsCompositeKey = CompositeKeyValue | CompositeKeyReferenceValue,
		IsIdentifier = IsCompositeKey | RowValueIdentifier | ColumnNameAsIdentifier
	}
}