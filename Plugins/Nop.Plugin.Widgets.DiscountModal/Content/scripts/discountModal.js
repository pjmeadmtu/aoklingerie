﻿var discountModal = function () {
	var $background = $('.mi-modal-background'),
		$modal = $background.find('.mi-modal'),
		$close = $background.find('.mi-modal-close a, .mi-no-thanks a'),
		$email = $background.find('input[type=\'email\']'),
		$button = $background.find('button'),
		$saving = $button.find('i.fa'),
		$errorLabel = $modal.find('p.error-label'),
		$formFields = $modal.find('div.form-field'),
		$thanks = $modal.find('.mi-modal-thanks-subscribing'),
		xsrfKey = $modal.find('input[name="__RequestVerificationToken"]').val(),
		delay = $background.find('input.delay').val(),
		discountModalId = $background.find('input.discount-modal-id').val(),
		showedModal = $.cookie('discount-modal-' + discountModalId),
		discountMessageService = new executeRequest('/mutableideas/discountmodal');

	var _showModal = function () {
		$background.show();
		$modal.fadeIn(500);
	};

	var _hideModal = function () {
		$modal.hide();
		$background.hide();
		$.cookie('discount-modal-' + discountModalId, true, {
			expires: 30,
			path: '/'
		});
	};

	var _init = function () {
		setTimeout(_showModal, delay);
		discountMessageService.intercept(function (options) {
			options.headers = {
				RequestVerificationToken: xsrfKey
			};

			options.complete = function () {
				$button.attr('disabled', false);
				$saving.toggle();
			};

			return options;
		});
	};

	var onSaved = function () {
		$formFields.hide();
		$thanks.show();

		setTimeout(_hideModal, 10000);
	};

	var onError = function (error) {
		var err = 'An unexpected error occurred while processing your request';

		switch (error.status) {
			case 400:
				err = error.responseText;
				break;
		}

		$button.attr('disabled', false);
		$errorLabel.text(err).show();
	};

	var _checkEmailIsValid = function () {
		if (isEmail($email.val())) {
			$errorLabel.hide();
			return true;
		}

		$errorLabel.text('Please enter a valid email address.');
		$errorLabel.show();
		return false;
	};

	var _onButtonClick = function () {
		$errorLabel.hide();
		if (!_checkEmailIsValid()) return;

		$button.attr('disabled', true);
		$saving.toggle();

		discountMessageService.post($email.val(), 'subscribe-discount').then(onSaved, onError);
	};

	$close.click(_hideModal);
	$button.click(_onButtonClick);

	if (!showedModal) {
		_init();
	}
};

$(function () {
	discountModal();
});
