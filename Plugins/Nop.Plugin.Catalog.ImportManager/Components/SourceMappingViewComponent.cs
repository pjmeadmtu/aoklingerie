﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "SourceMapping")]
	public class SourceMappingViewComponent : NopViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View($"{SharedPath}/SourceMapping.cshtml");
		}
	}
}