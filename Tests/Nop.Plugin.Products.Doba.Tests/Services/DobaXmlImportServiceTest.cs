﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nop.Plugin.Catalog.Doba.Services;
using Nop.Plugin.Catalog.Doba.Models;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Plugin.Catalog.Doba.Domain.Services;
using Nop.Services.Logging;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;

namespace Nop.Plugin.Catalog.Doba.Tests.Services
{
	[TestClass]
    public class DobaXmlImportServiceTest
    {
		DobaXmlImportService _importService;
		int _productId = 1;

		[TestInitialize]
		public void Initialize()
		{
			var productServiceMock = new Mock<IProductService>();
			productServiceMock.Setup(p => p.InsertProduct(It.IsAny<Product>())).Callback<Product>(product =>
			{
				_productId += 1;
				product.Id = _productId;
			});

			var pictureServiceMock = new Mock<IPictureService>();
			pictureServiceMock.Setup(p => p.InsertPicture(
				It.IsAny<byte[]>(),
				It.IsAny<string>(),
				It.IsAny<string>(), null, null, true, true)
			).Returns(new Picture());

			var httpServiceMock = new Mock<IHttpClient>();
			httpServiceMock.Setup(p => p.GetAsync(It.IsAny<string>()))
				.Returns(() => {
					StreamContent content = new StreamContent(new MemoryStream(new byte[] { }));
					return Task.FromResult(new HttpResponseMessage
					{
						Content = content
					});
				});

			var logServiceMock = new Mock<ILogger>();
			logServiceMock.Setup(p => p.InsertLog(LogLevel.Error, It.IsAny<string>(), It.IsAny<string>(), null));

			_importService = new DobaXmlImportService(
				productServiceMock.Object,
				pictureServiceMock.Object,
				logServiceMock.Object,
				httpServiceMock.Object
			);
		}

		[TestMethod]
		public void DeserializeXml()
		{
			DobaProductModelList productList = GetProductModelList();

			productList.Should().NotBeNull();
			productList.Products.Should().NotBeNull();
			productList.Products.Any().Should().BeTrue();
		}

		[TestMethod]
		public void Import()
		{
			IEnumerable<DobaProductModel> productList = GetProductModelList().Products.Take(5);
			IEnumerable<Product> importedProducts = null;

			_importService.Invoking(svc =>
			{
				importedProducts = _importService.ImportList(productList).GetAwaiter().GetResult();
			}).Should().NotThrow();

			importedProducts.Should().NotBeNullOrEmpty();
			importedProducts.Count().Should().Be(5);
		}

		DobaProductModelList GetProductModelList()
		{
			string currentFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Services", "TestXml", "drinking_games.xml");
			DobaProductModelList productList = null;

			using (StreamReader memStream = new StreamReader(currentFilePath))
			{
				_importService.Invoking(svc =>
				{
					productList = svc.ParseList(memStream.BaseStream);
				}).Should().NotThrow();
			}

			return productList;
		}
    }
}
