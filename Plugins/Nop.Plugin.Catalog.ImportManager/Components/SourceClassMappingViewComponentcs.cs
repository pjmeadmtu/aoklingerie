﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "SourceClassMappingView")]
	public class SourceClassMappingViewComponent : NopViewComponent
	{
		public SourceClassMappingViewComponent() { }

		public IViewComponentResult Invoke()
		{
			return View($"{SharedPath}/SourceClassMappingView.cshtml");
		}
	}
}