﻿using System.Linq;

using AutoMapper;

using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class ImportCollectionProfile : Profile
	{
		public ImportCollectionProfile()
		{
			CreateMap<ImportCollection, ImportCollectionModel>()
				.Map(p => p.ImportSources.Select(s => s.Id).ToArray(), p => p.ImportSources);

			CreateMap<ImportCollectionModel, ImportCollection>()
				.Map(p => p.Id.HasValue ? p.Id.Value : -1, p => p.Id)
				.Ignore(p => p.ImportSources);
		}
	}
}