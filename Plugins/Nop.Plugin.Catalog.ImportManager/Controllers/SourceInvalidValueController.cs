﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[Route("admin/mutableideas/source-invalid-value")]
	public class SourceInvalidValueController : BaseCrudController<SourceInvalidValue, SourceInvalidValueModel>
	{
		public SourceInvalidValueController(ICrudService<SourceInvalidValue, SourceInvalidValueModel> crudService)
			: base(crudService) { }
	}
}