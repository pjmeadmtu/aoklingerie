﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using Nop.Core;
using Nop.Plugin.Catalog.Doba.Domain.Models;

namespace Nop.Plugin.Catalog.Doba.Domain.Services.Data
{
    public interface ICrudService<T, TModel>
		where T : BaseEntity
		where TModel : BaseEntityModel
    {
		IEnumerable<TModel> Load();
		IEnumerable<TModel> Load(IEnumerable<int> ids);
		TModel GetById(int id);
		IEnumerable<TModel> Where(Expression<Func<T, bool>> expression);
		IEnumerable<TModel> Save(IEnumerable<TModel> models);

		TModel Insert(TModel model);
		TModel Update(TModel model);

		void Delete(int id);
		void Delete(IEnumerable<int> ids);
    }
}
