﻿var importSourceColumnsService = function(subscriptions, qsService) {
	var service = baseService('import-source-column', subscriptions);
	return $.extend(service, {
		getForSource: function (sourceId) {
			var qString = qsService.by('importSource.id', filterType.EQUAL, sourceId).build();
			return this.get("?filter=" + qString, 'GETFORSOURCE');
		}
	});
};