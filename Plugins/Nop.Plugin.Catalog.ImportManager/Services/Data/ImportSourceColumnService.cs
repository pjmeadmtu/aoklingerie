﻿using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class ImportSourceColumnService : BaseDataService<ImportSourceColumn, ImportSourceColumnModel>, IImportSourceColumnService
	{
		public ImportSourceColumnService(IImportSourceColumnRepository repository, IQueryStringExpressionService<ImportSourceColumn> querystringExpressionService, IMapper mapper)
			: base(repository, querystringExpressionService, mapper) { }
	}
}