﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[AuthorizeAdmin]
	[Area(AreaNames.Admin)]
	[Route("admin/mutableideas/source-key-column")]
	public class SourceKeyColumnController : BaseCrudController<SourceKeyColumn, SourceKeyColumnModel>
	{
		readonly ISourceKeyColumnService _sourceKeyColumnService;

		public SourceKeyColumnController(ISourceKeyColumnService crudService) : base(crudService)
		{
			_sourceKeyColumnService = crudService;
		}
	}
}
