﻿using System;
using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.DiscountModal
{
	public class DiscountModalSettings : ISettings
	{
		public int DiscountId { get; set; }
		public string Title { get; set; }
		public string ButtonText { get; set; }
		public string CancelMessage { get; set; }
		public string PlaceHolderText { get; set; }
		public string Message { get; set; }
		public string ClosingText { get; set; }
		public string CustomCss { get; set; }
		public string ThanksForSubscribing { get; set; }
		public int Delay { get; set; }
		public int CampaignId { get; set; }
		public DateTime LastUpdate { get; set; }
    }
}