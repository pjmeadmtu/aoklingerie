﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[Route("admin/mutableideas/import-collection")]
    public class ImportCollectionController : BaseCrudController<ImportCollection, ImportCollectionModel>
    {
		readonly IImportService _importService;

		public ImportCollectionController(IImportCollectionService service, IImportService importService) : base(service)
		{
			_importService = importService;
		}

		[HttpGet("import/{collectionId}")]
		public async Task<IActionResult> Import(int collectionId)
		{
			await Task.Run(() => _importService.ImportCollection(collectionId));
			return Ok();
		}
    }
}