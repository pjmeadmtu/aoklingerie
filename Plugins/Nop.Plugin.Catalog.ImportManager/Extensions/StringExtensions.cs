﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class StringExtensions
	{
		static IDictionary<Type, MethodInfo> _tryParseDict = new Dictionary<Type, MethodInfo>();
		static MethodInfo _convertGeneric = typeof(StringExtensions).GetRuntimeMethods().Where(p => p.Name == "Convert" && p.IsGenericMethod).FirstOrDefault();

		public static decimal ConvertToDecimal(this string value)
		{
			return Convert<decimal>(value);
		}

		public static string SourceKey(this string keyValue)
		{
			return keyValue.Trim().ToLower();
		}

		public static object Convert(this string value, Type itemType)
		{
			MethodInfo genericMethod = _convertGeneric.MakeGenericMethod(itemType);
			return genericMethod.Invoke(value, new[] { value });
		}

		public static T Convert<T>(this string value)
		{
			MethodInfo methodInfo;
			Type itemType = typeof(T);
			T output = default(T);
			object[] parameters = null;
			TypeCode typeCode = Type.GetTypeCode(typeof(T));

			switch (typeCode)
			{
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					methodInfo = GetNumberMethodInfo(itemType);
					parameters = new object[] { value, NumberStyles.Any, NumberFormatInfo.CurrentInfo, null };
					break;
				default:
					methodInfo = GetTryParse(itemType);
					parameters = new object[] { value, null };
					break;
			}

			if ((bool)methodInfo.Invoke(null, parameters))
				output = (T)parameters[parameters.Length - 1];

			return output;
		}

		static MethodInfo GetTryParse(Type itemType)
		{
			return GetTryParseMethodInfo(itemType, 2);
		}

		static MethodInfo GetNumberMethodInfo(Type itemType)
		{
			return GetTryParseMethodInfo(itemType, 4);
		}

		static MethodInfo GetTryParseMethodInfo(Type itemType, int parameterCount)
		{
			MethodInfo methodInfo;

			if (!_tryParseDict.TryGetValue(itemType, out methodInfo))
			{
				methodInfo = itemType.GetRuntimeMethods().Where(p => p.Name == "TryParse" && p.GetParameters().Count() == parameterCount).FirstOrDefault();
				_tryParseDict.AddOrUpdate(itemType, methodInfo);
			}

			return methodInfo;
		}

		/*

		static MethodInfo GetTryParse(Type itemType)
		{
			if (_tryParseDict.ContainsKey(itemType))
				return _tryParseDict[itemType];

			switch (Type.GetTypeCode(itemType))
			{
				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Char:
				case TypeCode.DateTime:
					return itemType.GetRuntimeMethods().Where(p => p.Name == "TryParse" && p.GetParameters().Count() == 2).FirstOrDefault();
				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					return itemType.GetRuntimeMethods().Where(p => p.Name == "TryParse" && p.GetParameters().Count() == 4).FirstOrDefault();
			}
		}

		 */
	}
}
