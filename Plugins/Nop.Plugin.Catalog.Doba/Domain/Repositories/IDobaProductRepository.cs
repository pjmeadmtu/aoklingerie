﻿using Nop.Core.Data;
using Nop.Plugin.Catalog.Doba.Domain.Entities;

namespace Nop.Plugin.Catalog.Doba.Domain.Repositories
{
    public interface IDobaProductRepository : IRepository<DobaProduct> { }
}