﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using ExcelDataReader;

using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Extensions;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

namespace Nop.Plugin.Catalog.ImportManager.Services
{
	public class ExcelExtractService : IExcelExtractService, IDisposable
	{
		readonly IImportSourceService _importSourceService;
		readonly ISourceClassMappingRepository _sourceClassMappingRepository;
		readonly ISourceMappingRefRepository _sourceMappingRefRepository;
		readonly IDictionary<Type, IEnumerable<PropertyInfo>> _dictProperties;
		readonly IDictionary<ImportEntityType, IDictionary<string, int>> _entityTypeDict;
		readonly IRepositoryFactory _repositoryFactory;
		readonly IKeyGeneratorService _keyGeneratorService;

		IExcelDataReader _excelDataReader;
		IDictionary<int, IEnumerable<ImportSourceColumn>> _sourceMappingDict;

		public ExcelExtractService(IImportSourceService sourceService,
			ISourceClassMappingRepository sourceClassMappingService,
			ISourceMappingRefRepository sourceMappingRefRepository,
			IKeyGeneratorService keyGeneratorService,
			IRepositoryFactory repositoryFactory)
		{
			_importSourceService = sourceService;
			_sourceClassMappingRepository = sourceClassMappingService;
			_sourceMappingRefRepository = sourceMappingRefRepository;
			_repositoryFactory = repositoryFactory;
			_keyGeneratorService = keyGeneratorService;
	
			_entityTypeDict = new Dictionary<ImportEntityType, IDictionary<string, int>>();
			_dictProperties = new Dictionary<Type, IEnumerable<PropertyInfo>>();
			_sourceMappingDict = new Dictionary<int, IEnumerable<ImportSourceColumn>>();
		}

		public IDictionary<string, T> Extract<T>(int importSourceMappingId)
			where T : BaseEntity, new()
		{
			IDictionary<string, T> entities = new Dictionary<string, T>();
			SourceClassMapping sourceClassMapping = _sourceClassMappingRepository.GetById(importSourceMappingId);
			
			// need to reference later in the key mappings
			_sourceMappingDict.AddOrUpdate(sourceClassMapping.SourceId, sourceClassMapping.ImportSource.ImportSourceColumns);

			using (Stream stream = _importSourceService.GetSourceStream(sourceClassMapping.SourceId))
			using (_excelDataReader = GetReaderByType(stream, sourceClassMapping.ImportSource.SourceType))
				ParseEntities<T>(sourceClassMapping).Iterate(p => entities.AddOrUpdate(p.Key, p.Value));

			return entities;
		}

		public void Dispose()
		{
			if (_excelDataReader != null || !_excelDataReader.IsClosed)
			{
				_excelDataReader.Dispose();
				_excelDataReader = null;
			}
		}

		T MapEntity<T>(IEnumerable<SourceMapping> sourceMappings,
			IEnumerable<SourceInvalidValue> invalidValues,
			string rowIdentifier,
			IDictionary<string, T> entitiesDict)
			where T : BaseEntity, new()
		{
			T entity = entitiesDict.ContainsKey(rowIdentifier) ? entitiesDict[rowIdentifier] : new T();
			IDictionary<string, PropertyInfo> properties = GetPropertiesDictionary<T>(sourceMappings);

			foreach (SourceMapping mapping in sourceMappings)
			{
				// if the items exits and we don't allow for overwrite continue
				if (!TryGetMapProperty(entity, mapping, invalidValues, out object stringValue))
				{
					if (mapping.ColumnType == ColumnType.ReferenceValue)
						return null;

					continue;
				}

				MapProperty(entity, properties[mapping.DestinationPropertyName], stringValue);
			}

			return entity;
		}

		bool TryGetMapProperty<T>(T entity, SourceMapping mapping, IEnumerable<SourceInvalidValue> invalidValues, out object stringValue)
			where T : BaseEntity, new()
		{
			stringValue = string.Empty;
			if (entity.Id == 0 || mapping.Overwrite)
				return IsValidMappingValue(mapping, invalidValues, out stringValue);

			return false;
		}

		bool IsValidMappingValue(SourceMapping sourceMapping, IEnumerable<SourceInvalidValue> invalidValues, out object stringValue)
		{
			stringValue = GetMappingValue(sourceMapping);

			switch (sourceMapping.ColumnType)
			{
				case ColumnType.ReferenceValue:
					return !stringValue.ToString().Equals("-1");
				case ColumnType.DefaultToValue:
					return true;
			}

			return HasValidRowValue(sourceMapping.ImportSourceColumn, invalidValues);
		}

		object GetMappingValue(SourceMapping mapping)
		{
			object value = null;

			switch (mapping.ColumnType)
			{
				case ColumnType.RowValue:
					value = _excelDataReader.GetValue(mapping.ImportSourceColumn.Index);
					break;
				case ColumnType.DefaultToValue:
					value = mapping.DefaultValue;
					break;
				case ColumnType.ColumnNameAsValue:
					value = mapping.ImportSourceColumn.Name;
					break;
				case ColumnType.ReferenceValue:
					value = GetReferenceValue(mapping);
					break;
			}

			return value.ToString();
		}

		int GetReferenceValue(SourceMapping sourceMapping)
		{
			string referenceKey = GetKeyValue(sourceMapping.ReferenceClassMapping.SourceKeyColumns);
			ImportEntityType referenceEntityType = sourceMapping.ReferenceClassMapping.EntityType;

			if (!_entityTypeDict.TryGetValue(referenceEntityType, out IDictionary<string, int> entitiesDictionary)
				|| !entitiesDictionary.ContainsKey(referenceKey))
				entitiesDictionary = GetEntitiesDictionary(sourceMapping.SourceClassMapping.SourceId, sourceMapping.ReferenceClassMapping.EntityType);

			return entitiesDictionary.TryGetValue(referenceKey, out int referenceId) ? referenceId : -1;
		}

		void MapProperty<T>(T entity, PropertyInfo property, object value)
			where T : BaseEntity
		{
			if (property.PropertyType == typeof(string))
			{
				string originalValue = property.GetValue(entity) as string;

				if (!string.IsNullOrEmpty(originalValue))
					value = string.Join(",", originalValue, value.ToString());
			}

			if (property.PropertyType.IsValueType)
				value = value.ToString().Convert(property.PropertyType);

			property.SetValue(entity, value);
		}

		IDictionary<string, PropertyInfo> GetPropertiesDictionary<T>(IEnumerable<SourceMapping> sourceMappings)
			where T : BaseEntity
		{
			return GetProperties<T>()
				.Where(property => sourceMappings.Any(p => p.DestinationPropertyName.Equals(property.Name)))
				.ToDictionary(p => p.Name, p => p);
		}

		IExcelDataReader GetReaderByType(Stream stream, SourceType sourceType)
		{
			CheckStreamCanBeReadAndSought(stream);
			stream.Position = 0;
			return (sourceType == SourceType.Excel ? ExcelReaderFactory.CreateReader(stream) : ExcelReaderFactory.CreateCsvReader(stream));
		}

		void CheckStreamCanBeReadAndSought(Stream stream)
		{
			if (!stream.CanRead || !stream.CanSeek)
				throw new InvalidOperationException();
		}

		IEnumerable<PropertyInfo> GetProperties<T>()
		{
			IEnumerable<PropertyInfo> properties;
			Type type = typeof(T);

			if (_dictProperties.TryGetValue(type, out properties))
				return properties;

			properties = ObjectExtensions.GetPublicReadWrite(typeof(T));
			_dictProperties.Add(type, properties);

			return properties;
		}

		IDictionary<string, T> ParseEntities<T>(SourceClassMapping sourceClassMapping)
			where T : BaseEntity, new()
		{
			int i = 0;
			string[] headerNames = { };
			IDictionary<string, T> mappedEntities = new Dictionary<string, T>();

			ImportSource importSource = sourceClassMapping.ImportSource;
			IEnumerable<SourceKeyColumn> sourceKeyColumns = sourceClassMapping.SourceKeyColumns.ToArray();
			IDictionary<string, T> existingEntities = GetReaderMappingRefs<T>(sourceClassMapping, sourceKeyColumns);
			
			_excelDataReader.Reset();
			while (_excelDataReader.Read())
			{
				if (i == 0 && sourceClassMapping.ImportSource.IsFirstRowHeader)
				{
					i++;
					continue;
				}

				string identifierValue = GetKeyValue(sourceKeyColumns);
				if (ShouldNotCreateEntity(identifierValue, mappedEntities, sourceKeyColumns, importSource.InvalidValues))
					continue;	

				T entity = MapEntity(sourceClassMapping.SourceMappings, importSource.InvalidValues, identifierValue, existingEntities);

				if (entity != null)
					mappedEntities.Add(identifierValue, entity);
			}

			return mappedEntities;
		}

		bool ShouldNotCreateEntity<T>(string identifier, IDictionary<string, T> mappedEntities, IEnumerable<SourceKeyColumn> sourceKeyColumns, IEnumerable<SourceInvalidValue> invalidValues)
		{
			return mappedEntities.ContainsKey(identifier)
				|| !HasValidSourceKeyValue(sourceKeyColumns, invalidValues);
		}

		bool HasValidSourceKeyValue(IEnumerable<SourceKeyColumn> sourceKeys, IEnumerable<SourceInvalidValue> invalidValues)
		{
			IEnumerable<SourceKeyColumn> rowKeys = sourceKeys.Where(p => (p.ColumnType == ColumnType.RowValue || p.ColumnType == ColumnType.CompositeKeyValue)
				&& p.SourceColumn != null).ToArray();

			// haven't taken into a account the regex
			IEnumerable<string> values = invalidValues.Select(p => p.Value).ToArray();

			return rowKeys.NotAny(key =>
			{
				string mappingValue = _excelDataReader.GetValue(key.SourceColumn.Index).ToString().Trim();
				return values.Any(value => value.Equals(mappingValue, StringComparison.OrdinalIgnoreCase));
			});
		}

		bool HasValidRowValue(ImportSourceColumn sourceColumn, IEnumerable<SourceInvalidValue> invalidValues)
		{
			if (sourceColumn == null) return false;

			string rowValue = _excelDataReader.GetValue(sourceColumn.Index).ToString().Trim();
			return !string.IsNullOrEmpty(rowValue) && invalidValues.NotAny(p => p.Value.Equals(rowValue, StringComparison.OrdinalIgnoreCase));
		}

		IDictionary<string, T> GetReaderMappingRefs<T>(SourceClassMapping sourceClassMapping, IEnumerable<SourceKeyColumn> sourceKeyColumns)
			where T : BaseEntity
		{
			int i = 0;
			List<string> sourceIdentifiers = new List<string>();
			ImportSource importSource = sourceClassMapping.ImportSource;

			_excelDataReader.Reset();
			while (_excelDataReader.Read())
			{
				if (i == 0 && sourceClassMapping.ImportSource.IsFirstRowHeader)
				{
					i++;
					continue;
				}

				string keyName = GetKeyValue(sourceClassMapping.SourceKeyColumns);
				if (!string.IsNullOrEmpty(keyName) && !sourceIdentifiers.Contains(keyName))
					sourceIdentifiers.Add(keyName);

				i++;
			}

			return GetMappingRefs<T>(sourceIdentifiers, sourceClassMapping);
		}

		IDictionary<string, T> GetMappingRefs<T>(IEnumerable<string> sourceIdentifiers, SourceClassMapping classMapping)
			where T : BaseEntity
		{
			IDictionary<int, string> refEntityDictionary = GetCollectionEntityIds(classMapping.ImportSource.Id, classMapping.EntityType);
			IRepository<T> repository;

			repository = _repositoryFactory.GetRepository<T>();

			return repository.Where(p => refEntityDictionary.Keys.Contains(p.Id))
				.ToDictionary(p => refEntityDictionary[p.Id], p => p);
		}

		IDictionary<int, string> GetCollectionEntityIds(int sourceId, ImportEntityType entityType)
		{
			IDictionary<int, string> refEntityDictionary = _sourceMappingRefRepository.GetForSourceAndEntityType(sourceId, entityType)
			.ToDictionary(p => p.EntityId, p => p.SourceIdValue);

			return refEntityDictionary;
		}

		IDictionary<string, int> GetEntitiesDictionary(int sourceId, ImportEntityType entityType)
		{
			IDictionary<string, int> dict = _sourceMappingRefRepository.GetForSourceAndEntityType(sourceId, entityType)
				.ToDictionary(p => p.SourceIdValue, p => p.EntityId);

			_entityTypeDict.AddOrUpdate(entityType, dict);
			return dict;
		}

		string GetKeyValue(IEnumerable<SourceKeyColumn> sourceKeyColumns)
		{
			return _keyGeneratorService.GenerateKey(sourceKeyColumns, GetReaderKeyValue);
		}

		object GetReaderKeyValue(ImportSourceColumn sourceColumn, ColumnType columnType)
		{
			switch (columnType)
			{
				case ColumnType.ColumnNameAsIdentifier:
					return sourceColumn.Name;
				case ColumnType.RowValue:
					return _excelDataReader.GetValue(sourceColumn.Index);
			}

			throw new InvalidOperationException("Only ColumnNameAsIdentifier or RowValue may be used to retrieve the key value");
		}
	}
}