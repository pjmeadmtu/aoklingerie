﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Autofac;
using Autofac.Core;
using AutoMapper;

using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Catalog.ImportManager.Infrastructure.Database;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Web.Framework.Infrastructure;
using Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories;
using Nop.Plugin.Catalog.ImportManager.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using MutableIdeas.Web.Linq.Query.Service;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.IOC
{
	public class DependencyRegistrar : IDependencyRegistrar
	{
		public int Order => 1;

		public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
		{
			var dataSettingsManager = new DataSettingsManager();
			DataSettings dataSettings = dataSettingsManager.LoadSettings();

			this.RegisterPluginDataContext<ImportManagerContext>(builder, "nop_object_context_catalog_import_manager");
			ResolvedParameter resolvedParameter = ResolvedParameter.ForNamed<IImportManagerContext>("nop_object_context_catalog_import_manager");

			builder.Register<IImportManagerContext>(c => new ImportManagerContext(dataSettings.DataConnectionString)).InstancePerLifetimeScope();

			builder.RegisterType<ImportCollectionRepository>().As<IImportCollectionRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<ImportSourceRepository>().As<IImportSourceRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<SourceClassMappingRepository>().As<ISourceClassMappingRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<SourceMappingRepository>().As<ISourceMappingRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<SourceMappingRefRepository>().As<ISourceMappingRefRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<ImportSourceColumnRepository>().As<IImportSourceColumnRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<SourceInvalidValueRepository>().As<ISourceInvalidValueRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<SourceKeyColumnRepository>().As<ISourceKeyColumnRepository>()
				.WithParameter(resolvedParameter)
				.InstancePerLifetimeScope();

			builder.RegisterType<ImportCollectionService>().As<IImportCollectionService>().InstancePerLifetimeScope();
			builder.RegisterType<ImportSourceService>().As<IImportSourceService>().InstancePerLifetimeScope();
			builder.RegisterType<SourceClassMappingService>().As<ISourceClassMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<SourceMappingService>().As<ISourceMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<SourceMappingRefService>().As<ISourceMappingRefService>().InstancePerLifetimeScope();
			builder.RegisterType<ImportSourceColumnService>().As<IImportSourceColumnService>().InstancePerLifetimeScope();
			builder.RegisterType<SourceInvalidValueService>().As<ISourceInvalidValueService>().InstancePerLifetimeScope();
			builder.RegisterType<SourceKeyColumnService>().As<ISourceKeyColumnService>().InstancePerLifetimeScope();

			builder.RegisterType<ImportService>().As<IImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductImportService>().As<IProductImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductAttributeImportService>().As<IProductAttributeImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductAttributeValueImportService>().As<IProductAttributeValueImportService>().InstancePerLifetimeScope();
			builder.RegisterType<SpecificationAttributeImportService>().As<ISpecificationAttributeImportService>().InstancePerLifetimeScope();
			builder.RegisterType<SpecificationAttributeOptionImportService>().As<ISpecificationAttributeOptionImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductAttributeMappingImportService>().As<IProductAttributeMappingImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductSpecificationAttributeImportService>().As<IProductSpecificationAttributeImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductTagImportService>().As<IProductTagImportService>().InstancePerLifetimeScope();
			builder.RegisterType<CategoryImportService>().As<ICategoryImportService>().InstancePerLifetimeScope();
			builder.RegisterType<CategoryMapImportService>().As<ICategoryMapImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductAttributeCombinationImportService>().As<IProductAttributeCombinationImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductAttributeCombinationImportParserService>().As<IProductAttributeCombinationImportParserService>().InstancePerLifetimeScope();

			builder.RegisterType<KeyGeneratorService>().As<IKeyGeneratorService>().InstancePerLifetimeScope();
			builder.RegisterType<ExcelExtractService>().As<IExcelExtractService>().InstancePerLifetimeScope();
			
			builder.RegisterGeneric(typeof(QueryStringExpressionService<>)).As(typeof(IQueryStringExpressionService<>)).InstancePerLifetimeScope();
			builder.RegisterGeneric(typeof(FilterService<>)).As(typeof(IFilterService<>)).InstancePerLifetimeScope();

			builder.Register<IRepositoryFactory>(ctx =>
			{
				var context = ctx.Resolve<IComponentContext>();
				return new RepositoryFactory(context);
			}).As<IRepositoryFactory>();

			builder.Register<IImportEntityServiceFactory>(ctx =>
			{
				var context = ctx.Resolve<IComponentContext>();
				return new ImportEntityServiceFactory(context);
			}).As<IImportEntityServiceFactory>();

			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			builder.RegisterAssemblyTypes(assemblies)
				.Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract && t.IsPublic)
				.As<Profile>();

			builder.Register(c => new MapperConfiguration(cfg => {
				foreach (var profile in c.Resolve<IEnumerable<Profile>>())
				{
					cfg.AddProfile(profile);
				}
			})).AsSelf().SingleInstance();

			builder.Register(c => c.Resolve<MapperConfiguration>()
				.CreateMapper(c.Resolve))
				.As<IMapper>()
				.InstancePerLifetimeScope();
		}
	}
}