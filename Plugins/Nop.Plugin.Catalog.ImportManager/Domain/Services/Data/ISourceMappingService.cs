﻿using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface ISourceMappingService : ICrudService<SourceMapping, SourceMappingModel>
	{
		IEnumerable<string> GetPropertyNames(ImportEntityType entityType);
	}
}