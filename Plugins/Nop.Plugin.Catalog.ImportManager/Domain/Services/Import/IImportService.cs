﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Import
{
	public interface IImportService
	{
		void ImportCollection(int collectionId);
	}
}