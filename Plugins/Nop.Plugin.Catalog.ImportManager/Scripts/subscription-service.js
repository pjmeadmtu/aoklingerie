﻿var collectionEventSubscriptions = function () {
	var subscriptions = {};

	var watch = function (evtName, handler) {
		if ($.isArray(evtName)) {
			var length = evtName.length;
			for (var i = 0; i < length; i++) {
				watch(evtName[i], handler);
			}
			return;
		}

		if (subscriptions[evtName] === undefined) {
			subscriptions[evtName] = [];
		}
		subscriptions[evtName].push(handler);
	};

	var push = function (evtName, value) {
		var subscribers = subscriptions[evtName];
		if (subscribers === undefined) return;

		var length = subscribers.length;
		for (var i = 0; i < length; i++) {
			subscriptions[evtName][i](value);
		}
	};

	return {
		watch: watch,
		push: push
	};
};