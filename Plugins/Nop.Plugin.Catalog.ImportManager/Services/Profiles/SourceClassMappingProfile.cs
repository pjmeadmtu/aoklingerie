﻿using System.Linq;
using Nop.Plugin.Catalog.ImportManager.Extensions;
using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class SourceClassMappingProfile : Profile
	{
		public SourceClassMappingProfile()
		{
			CreateMap<SourceClassMapping, SourceClassMappingModel>()
				.Map(p => p.SourceMappings.Select(i => i.Id).ToArray(), p => p.SourceMappings)
				.Map(p => p.SourceKeyColumns.Select(i => i.Id).ToArray(), p => p.SourceKeyMappings);

			CreateMap<SourceClassMappingModel, SourceClassMapping>()
				.Ignore(p => p.ImportSource)
				.Ignore(p => p.SourceMappings)
				.Ignore(p => p.SourceKeyColumns);
		}
	}
}