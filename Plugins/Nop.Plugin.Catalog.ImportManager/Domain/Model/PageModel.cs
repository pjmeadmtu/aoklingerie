﻿using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class PageModel<TModel>
	   where TModel : BaseEntityModel
	{
		public int Page { get; set; }
		public int Total { get; set; }
		public IEnumerable<TModel> Models { get; set; }
	}
}