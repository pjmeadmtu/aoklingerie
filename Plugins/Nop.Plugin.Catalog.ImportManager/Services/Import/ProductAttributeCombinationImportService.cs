﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Extensions;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductAttributeCombinationImportService : BaseImportService<ProductAttributeCombination>, IProductAttributeCombinationImportService
	{
		readonly ISourceMappingRefService _sourceMappingRefService;
		readonly IProductAttributeCombinationImportParserService _parserService;
		readonly IProductAttributeService _productAttributeService;
		readonly IExcelExtractService _excelExtractService;

		public ProductAttributeCombinationImportService(IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			IProductAttributeService productAttributeService,
			IProductAttributeCombinationImportParserService parserService)
			: base(excelExtractService, sourceMappingRefService,
				  ImportEntityType.ProductAttributeCombination,
				  productAttributeService.InsertProductAttributeCombination,
				  productAttributeService.UpdateProductAttributeCombination)
		{
			_parserService = parserService;
			_productAttributeService = productAttributeService;
			_sourceMappingRefService = sourceMappingRefService;
			_excelExtractService = excelExtractService;
		}

		public override void Import(SourceClassMappingModel sourceClassMappingModel)
		{
			var sourceMappingRefModels = new List<SourceMappingRefModel>();
			IDictionary<string, ProductAttributeCombination> dictEntities = _excelExtractService.Extract<ProductAttributeCombination>(sourceClassMappingModel.Id.Value);
			IEnumerable<string> sourceIdValues = _sourceMappingRefService.GetBySourceAndEntity(sourceClassMappingModel.SourceId,
				ImportEntityType.ProductAttributeCombination).Select(p => p.SourceIdValue).ToArray();

			dictEntities.Keys.Iterate(key =>
			{
				ProductAttributeCombination entity = dictEntities[key];
				if (entity.Id == 0 && !string.IsNullOrEmpty(entity.AttributesXml))
					entity.AttributesXml = ParseXml(sourceClassMappingModel.SourceId, entity);

				SaveEntity(entity);
				if (sourceIdValues.Contains(key)) return;

				sourceMappingRefModels.Add(new SourceMappingRefModel
				{
					EntityId = entity.Id,
					SourceId = sourceClassMappingModel.SourceId,
					EntityType = ImportEntityType.ProductAttributeCombination,
					SourceIdValue = key
				});
			});

			_sourceMappingRefService.Save(sourceMappingRefModels);
		}

		string ParseXml(int sourceId, ProductAttributeCombination productAttributeCombination)
		{
			IEnumerable<int> values = productAttributeCombination.AttributesXml.Split(new[] { "," }, StringSplitOptions.None).Select(p => Convert.ToInt32(p)).ToArray();
			return _parserService.ParseAttributes(productAttributeCombination.ProductId, sourceId, values);
		}
	}
}