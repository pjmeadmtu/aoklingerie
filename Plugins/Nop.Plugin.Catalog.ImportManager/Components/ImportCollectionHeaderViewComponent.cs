﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "ImportCollectionHeader")]
	public class ImportCollectionHeaderViewComponent : NopViewComponent
	{
		readonly string _viewPath;

		public ImportCollectionHeaderViewComponent()
		{
			_viewPath = $"{SharedPath}/ImportCollectionHeader.cshtml";
		}

		public IViewComponentResult Invoke(ImportCollectionModel model)
		{
			return View(_viewPath, model);
		}
	}
}