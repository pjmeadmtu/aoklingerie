﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Enums
{
    public enum ImportEntityType
    {
		Product = 0,
		ProductPicture = 1,
		Inventory = 2,
		ProductAttribute = 3,
		ProductAttributeValue = 4,
		SpecificationAttribute = 5,
		SpecificationAttributeOption = 6,
		ProductAttributeMapping = 7,
		ProductSpecificationMapping = 8,
		ProductTag = 9,
		Category = 10,
		CategoryMap = 11,
		ProductAttributeCombination = 12
	}
}