﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "FieldMapping")]
	public class FieldMappingListViewComponent : NopViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View($"{SharedPath}/FieldMappingList.cshtml");
		}
	}
}