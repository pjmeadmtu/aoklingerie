﻿using System;

using Autofac;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ImportEntityServiceFactory : IImportEntityServiceFactory
	{
		readonly IComponentContext _componentContext;

		public ImportEntityServiceFactory(IComponentContext componentContext)
		{
			_componentContext = componentContext;
		}

		public IEntityImportService GetImportService(ImportEntityType entityType)
		{
			switch (entityType)
			{
				case ImportEntityType.Product:
					return _componentContext.Resolve<IProductImportService>();
				case ImportEntityType.ProductAttribute:
					return _componentContext.Resolve<IProductAttributeImportService>();
				case ImportEntityType.ProductAttributeValue:
					return _componentContext.Resolve<IProductAttributeValueImportService>();
				case ImportEntityType.SpecificationAttribute:
					return _componentContext.Resolve<ISpecificationAttributeImportService>();
				case ImportEntityType.SpecificationAttributeOption:
					return _componentContext.Resolve<ISpecificationAttributeOptionImportService>();
				case ImportEntityType.ProductAttributeMapping:
					return _componentContext.Resolve<IProductAttributeMappingImportService>();
				case ImportEntityType.ProductSpecificationMapping:
					return _componentContext.Resolve<IProductSpecificationAttributeImportService>();
				case ImportEntityType.ProductTag:
					return _componentContext.Resolve<IProductTagImportService>();
				case ImportEntityType.Category:
					return _componentContext.Resolve<ICategoryImportService>();
				case ImportEntityType.CategoryMap:
					return _componentContext.Resolve<ICategoryMapImportService>();
				case ImportEntityType.ProductAttributeCombination:
					return _componentContext.Resolve<IProductAttributeCombinationImportService>();
				default:
					throw new InvalidOperationException($"{Enum.GetName(typeof(ImportEntityType), entityType)} does not have an import service.");
			}
		}
	}
}