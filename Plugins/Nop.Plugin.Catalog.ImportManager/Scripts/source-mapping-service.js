﻿var propertyMappingService = function (collectionSubscriptions, qsService) {
	var service = new baseService('source-mapping', collectionSubscriptions);
	return $.extend(service, {
		getPropertyNames: function (entityType) {
			return this.get('property-names/' + entityType, 'GETPROPERTIES');
		},
		getForClasssMap: function (classMapId) {
			var qsString = qsService.by('sourceClassMapping.id', filterType.EQUAL, classMapId).build();
			return this.get("?filter=" + qsString, 'GETBYCLASS');
		}
	});
};