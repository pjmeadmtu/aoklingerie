﻿using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface ISourceMappingRefService : ICrudService<SourceMappingRef, SourceMappingRefModel>
	{
		IEnumerable<SourceMappingRefModel> GetBySourceAndEntity(int sourceId, ImportEntityType entityType);
	}
}