﻿var columnType = {
	RowValue: 1,
	RowValueIdentifier: 2,
	ColumnNameAsIdentifier: 4,
	DefaultToValue: 8,
	ReferenceValue: 16,
	CompositeKeyReferenceValue: 32,
	CompositeKeyValue: 64,
	IsIdentifier: [ this.RowValueIdentifier, this.CompositeKeyReferenceValue, this.CompositeKeyValue ],
	IsCompositeKey: [ this.CompositeKeyReferenceValue, this.CompositeKeyValue ]
};