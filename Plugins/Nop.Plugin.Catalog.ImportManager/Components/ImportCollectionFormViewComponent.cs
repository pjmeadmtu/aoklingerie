﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "ImportCollectionForm")]
	public class ImportCollectionFormViewComponent : NopViewComponent
	{
		readonly string _viewPath;

		public ImportCollectionFormViewComponent()
		{
			_viewPath = $"{SharedPath}/ImportCollectionForm.cshtml";
		}

		public IViewComponentResult Invoke(ImportCollectionModel model)
		{
			return View(_viewPath, model);
		}
	}
}