﻿using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;

using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductSpecificationAttributeImportService : BaseImportService<ProductSpecificationAttribute>, IProductSpecificationAttributeImportService
	{
		public ProductSpecificationAttributeImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			ISpecificationAttributeService specificationAttributeService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.ProductSpecificationMapping,
				  specificationAttributeService.InsertProductSpecificationAttribute,
				  specificationAttributeService.UpdateProductSpecificationAttribute) { }
	}
}