﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

using AutoMapper;
using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;

namespace Nop.Plugin.Catalog.Doba.Services.Import
{
	public class DobaXmlImportService : BaseImportService
	{
		readonly IMapper _mapper;

		public DobaXmlImportService(IMapper mapper, IDobaProductService dobaProductService)
			: base(mapper, dobaProductService)
		{
			_mapper = mapper;
		}

		protected async override Task<IEnumerable<DobaProductImportModel>> ParseList(Stream stream)
		{
			if (!stream.CanRead)
				throw new InvalidOperationException("Must be able to read stream.");

			using (MemoryStream memStream = new MemoryStream())
			{
				await stream.CopyToAsync(memStream);
				stream.Position = 0;
				XmlSerializer xmlSerializer = new XmlSerializer(typeof(DobaProductModelList));
				DobaProductModelList productModelList = xmlSerializer.Deserialize(stream) as DobaProductModelList;

				return _mapper.Map<IEnumerable<DobaProductImportModel>>(productModelList.Products);
			}
		}
	}
}