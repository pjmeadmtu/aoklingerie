﻿using Nop.Data;
using Nop.Plugin.Catalog.Doba.Domain.Database;
using Nop.Plugin.Catalog.Doba.Domain.Entities;
using Nop.Plugin.Catalog.Doba.Domain.Repositories;

namespace Nop.Plugin.Catalog.Doba.Infrastructure.Database.Repositories
{
	public class DobaProductRepository : EfRepository<DobaProduct>, IDobaProductRepository
	{
		public DobaProductRepository(IDobaDbContext context) : base(context) { }
	}
}
