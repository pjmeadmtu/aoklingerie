﻿var importSourceService = function (collectionSubscriptions) {
	var service = new baseService('import-source', collectionSubscriptions);
	return $.extend(service, {
		getByParent: function (parentId) {
			return this.get('parent/' + parentId, 'GETBYPARENT');
		},
		saveSource: function (data) {
			var url = [this.baseUrl, 'save-source'].join('/');
			$.ajax({
				url: url,
				contentType: false,
				processData: false,
				data: data,
				type: 'POST',
				success: function (result) {
					collectionSubscriptions.push(collectionTabsEvent.SOURCEMAPPINGSAVESOURCE, result);
				},
				error: this.pushError
			}).done(function () {
				collectionSubscriptions.push(collectionTabsEvent.SOURCEMAPPINGSAVESOURCEDONE);
			});
		}
	});
};