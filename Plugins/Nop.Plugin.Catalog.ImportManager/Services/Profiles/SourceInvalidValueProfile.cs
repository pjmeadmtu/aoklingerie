﻿using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class SourceInvalidValueProfile : Profile
	{
		public SourceInvalidValueProfile()
		{
			CreateMap<SourceInvalidValue, SourceInvalidValueModel>();
			CreateMap<SourceInvalidValueModel, SourceInvalidValue>()
				.Ignore(p => p.ImportSource);
		}
	}
}