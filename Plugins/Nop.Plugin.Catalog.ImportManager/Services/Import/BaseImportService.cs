﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services
{
	public abstract class BaseImportService<T> : IEntityImportService
		where T : BaseEntity, new()
	{
		readonly IExcelExtractService _excelExtractService;
		readonly ISourceMappingRefService _sourceMappingRefService;
		readonly ImportEntityType _importEntityType;
		readonly Action<T> _insert;
		readonly Action<T> _update;

		protected BaseImportService(IExcelExtractService excelExtractService, ISourceMappingRefService sourceMappingRefService, ImportEntityType entityType, Action<T> insert, Action<T> update)
		{
			_excelExtractService = excelExtractService;
			_sourceMappingRefService = sourceMappingRefService;
			_importEntityType = entityType;

			_insert = insert;
			_update = update;
		}

		protected virtual void SaveEntity(T entity)
		{
			if (entity.Id == 0)
			{
				_insert(entity);
				return;
			}

			_update(entity);
		}

		public virtual void Import(SourceClassMappingModel sourceClassMappingModel)
		{
			List<SourceMappingRefModel> sourceMappingRefModels = new List<SourceMappingRefModel>();

			IDictionary<string, T> dictEntities = _excelExtractService.Extract<T>(sourceClassMappingModel.Id.Value);
			IEnumerable<string> sourceRefModelIds = _sourceMappingRefService.Filter(p => p.SourceId == sourceClassMappingModel.SourceId
				&& p.EntityType == _importEntityType)
				.Select(p => p.SourceIdValue)
				.ToArray();

			dictEntities.Keys.Iterate(key =>
			{
				T entity = dictEntities[key];
				SaveEntity(entity);

				if (sourceRefModelIds.Contains(key)) return;

				sourceMappingRefModels.Add(new SourceMappingRefModel
				{
					EntityId = entity.Id,
					SourceId = sourceClassMappingModel.SourceId,
					EntityType = _importEntityType,
					SourceIdValue = key
				});
			});

			_sourceMappingRefService.Save(sourceMappingRefModels);
		}
	}
}
