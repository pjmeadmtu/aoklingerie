﻿using System.Collections.Generic;
using System.Linq;
using ExcelDataReader;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class IExcelDataReaderExtensions
	{
		/// <summary>
		/// Gets the header values and will reset
		/// </summary>
		/// <param name="dataReader"></param>
		/// <returns></returns>
		public static IEnumerable<string> GetHeaderValues(this IExcelDataReader dataReader)
		{
			List<string> headerValues = new List<string>();

			dataReader.Reset();
			dataReader.Read();

			headerValues = Enumerable.Range(0, dataReader.FieldCount)
				.Select(index => dataReader.GetValue(index) as string).ToList();

			dataReader.Reset();
			return headerValues;
		}
	}
}