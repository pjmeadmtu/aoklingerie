﻿namespace Nop.Plugin.Widgets.DiscountModal.Models
{
	public class DiscountModalSettingsModel : DiscountModalSettings
	{
		public bool ShouldDisplayModal { get; set; }
	}
}