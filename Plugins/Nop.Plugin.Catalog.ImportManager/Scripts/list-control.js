﻿var listControl = function ($sourceList, $btnAdd, $btnDelete, sourceRestService, events, onDataBound) {
	var $checkboxes = undefined,
		$masterCheckbox = undefined,
		selectedIds = [],
		$buttons = [];

	var setMasterChecked = function () {
		var disabled = selectedIds.length > 0 ? false : true;
		$masterCheckbox.attr('checked', $checkboxes.length > 0 && selectedIds.length === $checkboxes.length);
		$btnDelete.attr("disabled", disabled);
	};

	var onCheckboxChanged = function () {
		var $checkbox = jQuery(this);
		var isChecked = $checkbox.is(":checked");
		var value = parseInt($checkbox.val());

		if (!isChecked) {
			$masterCheckbox.attr('checked', false);

			selectedIds = selectedIds.filter(function (id) {
				return id !== value;
			});

		} else if (jQuery.inArray(value, selectedIds) === -1) {
			selectedIds.push(value);
		}

		setMasterChecked();
	};

	// need to figure out when to enable and disabled these buttons
	var disableButtons = function (disable) {
		if (disable) {
			for (var i = 0; i < $buttons.length; i++) {
				$buttons[i].attr('disabled', true)
			}
			return;
		}

		for (var i = 0; i < $buttons.length; i++) {
			$buttons[i].removeAttr('disabled');
		}
	};

	var deleteSelectedClick = function () {
		disableButtons(true);
		sourceRestService.removeMany(selectedIds);
	};

	var addClick = function () {
		collectionSubscriptions.push(events.toggleForm);
	};

	var refreshGrid = function () {
		$sourceList.data('kendoGrid').dataSource.read();
		disableButtons();
	};

	var masterCheckboxChange = function () {
		var isChecked = $masterCheckbox.is(":checked");
		var checked = isChecked ? "checked" : false;

		if ($checkboxes) {
			$checkboxes.attr("checked", checked).change();
		}
	};

	var sourceListDatabound = function () {
		selectedIds = [];

		$masterCheckbox = $sourceList.find("input.checkbox-master");
		$checkboxes = $sourceList.find("input[type='checkbox']").not($masterCheckbox);
		$checkboxes.change(onCheckboxChanged);
		$buttons = [$btnAdd, $btnDelete];
		$masterCheckbox.change(masterCheckboxChange);

		setMasterChecked();
		setupEditButton();

		if (onDataBound) onDataBound();
	};

	var editButtonClick = function () {
		var value = $(this).val();
		var dataItem = getById(value);

		if (dataItem === null) return;

		collectionSubscriptions.push(events.selected, dataItem);
		collectionSubscriptions.push(events.toggleForm);
	};

	var setupEditButton = function () {
		$editButton = $sourceList.find('.edit-button');
		$editButton.click(editButtonClick);
	};

	var getById = function (id) {
		var source = $sourceList.data('kendoGrid').dataSource,
			length = source._total;

		for (var i = 0; i < length; i++) {
			var dataItem = source.at(i);
			if (dataItem.Id === parseInt(id)) {
				return dataItem;
			}
		}

		return null;
	};

	$btnAdd.click(addClick);
	$btnDelete.click(deleteSelectedClick);
	$sourceList.data("kendoGrid").bind("dataBound", sourceListDatabound);

	collectionSubscriptions.watch(events.refreshGrid, refreshGrid);

	return {
		getById: getById
	};
};