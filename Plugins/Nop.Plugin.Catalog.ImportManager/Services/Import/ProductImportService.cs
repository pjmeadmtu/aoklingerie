﻿using System;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductImportService : BaseImportService<Product>, IProductImportService
	{
		public ProductImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			IProductService productService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.Product,
				  prod => {
					  prod.CreatedOnUtc = DateTime.UtcNow;
					  prod.UpdatedOnUtc = DateTime.UtcNow;
					  productService.InsertProduct(prod);
				  },
				  prod => {
					  prod.CreatedOnUtc = DateTime.UtcNow;
					  productService.UpdateProduct(prod);
				  }) { }
	}
}