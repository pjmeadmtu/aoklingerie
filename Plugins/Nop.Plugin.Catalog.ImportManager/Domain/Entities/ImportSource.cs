﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class ImportSource : BaseEntity
	{
		public int ImportCollectionId { get; set; }
		public string Name { get; set; }
		public SourceType SourceType { get; set; }
		public SourceType? SubSourceType { get; set; }
		public string Url { get; set; }
		public bool IsFirstRowHeader { get; set; }
		public DateTime? LastImport { get; set; }
		public byte[] Source { get; set; }

		public virtual ImportCollection ImportCollection { get; set; }
		public virtual ICollection<SourceClassMapping> SourceClassMappings { get; set; }
		public virtual ICollection<ImportSourceColumn> ImportSourceColumns { get; set; }
		public virtual ICollection<SourceInvalidValue> InvalidValues { get; set; }
		public virtual ICollection<SourceMappingRef> SourceMappingRefs { get; set; }
 	}
}