﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
    public static class ObjectExtensions
    {
		public static IEnumerable<PropertyInfo> GetPublicReadWrite(Type itemType)
		{
			return itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && p.CanWrite);
		}
    }
}