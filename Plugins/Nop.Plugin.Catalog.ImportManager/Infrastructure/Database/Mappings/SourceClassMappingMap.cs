﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
    public class SourceClassMappingMap : NopEntityTypeConfiguration<SourceClassMapping>
	{
		public SourceClassMappingMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}SourceClassMapping");
			HasKey(p => p.Id);
			HasRequired(p => p.ImportSource)
				.WithMany(p => p.SourceClassMappings)
				.HasForeignKey(p => p.SourceId);

			HasMany(p => p.SourceMappings)
				.WithRequired(p => p.SourceClassMapping)
				.HasForeignKey(p => p.SourceClassMappingId)
				.WillCascadeOnDelete(true);

			HasMany(p => p.SourceKeyColumns)
				.WithRequired(p => p.SourceClassMapping)
				.HasForeignKey(p => p.SourceClassMappingId)
				.WillCascadeOnDelete(true);

			Property(p => p.Name).IsRequired().HasMaxLength(100);
			Property(p => p.EntityType).IsRequired();
		}
    }
}