﻿var sourceClassMappingService = function (collectionSubscriptions, qsService) {
	var service = new baseService('source-class-mapping', collectionSubscriptions);

	return $.extend(service, {
		getByCollection: function (collectionId) {
			return this.get('by-collection/' + collectionId, collectionTabsEvent.CLASSMAPPINGGETBYCOLLECTION);
		},
		getForSource: function (sourceId) {
			var qString = qsService.by('sourceId', filterType.EQUAL, sourceId).build();
			return this.get('?filter=' + qString, 'GETBYSOURCEID');
		}
	});
};