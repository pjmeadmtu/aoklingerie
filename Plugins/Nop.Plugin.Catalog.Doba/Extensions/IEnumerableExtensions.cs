﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.Doba.Extensions
{
	public static class IEnumerableExtensions
	{
		public static void Iterate<T>(this IEnumerable<T> items, Action<T> action)
		{
			foreach (T item in items)
				action(item);
		}
	}
}
