﻿using System.Collections.Generic;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
    public interface ISourceClassMappingService : ICrudService<SourceClassMapping, SourceClassMappingModel>
	{
		IEnumerable<SourceClassMappingModel> GetAllForCollection(int collectionId);
	}
}