﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class SourceKeyColumnMap : NopEntityTypeConfiguration<SourceKeyColumn>
	{
		public SourceKeyColumnMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}SourceKeyColumn");

			HasRequired(p => p.SourceColumn)
				.WithMany(p => p.SourceColumnKeys)
				.HasForeignKey(p => p.SourceColumnId)
				.WillCascadeOnDelete(true);

			HasRequired(p => p.SourceClassMapping)
				.WithMany(p => p.SourceKeyColumns)
				.HasForeignKey(p => p.SourceClassMappingId)
				.WillCascadeOnDelete(true);

			HasOptional(p => p.ReferenceClassMapping)
				.WithMany()
				.HasForeignKey(p => p.ReferenceClassMappingId)
				.WillCascadeOnDelete(false);

			Property(p => p.Name).IsRequired();
			Property(p => p.ColumnType).IsRequired();
		}
	}
}