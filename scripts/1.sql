IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'DobaItemId'
    AND Object_ID = Object_ID(N'dbo.Product')) BEGIN

	ALTER TABLE dbo.Product
	ADD DobaItemId VARCHAR(400);

END;