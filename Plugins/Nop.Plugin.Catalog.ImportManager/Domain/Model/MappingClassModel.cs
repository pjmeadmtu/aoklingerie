﻿using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
    public class MappingClassModel
    {
		public string DisplayName { get; set; }
		public IEnumerable<string> Properties { get; set; }
		public string FullyQualifiedName { get; set; }
    }
}