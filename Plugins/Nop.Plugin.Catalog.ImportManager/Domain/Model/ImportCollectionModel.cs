﻿using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class ImportCollectionModel : BaseEntityModel
	{
		public ImportCollectionModel()
		{
			ImportSources = new List<int>();
			SourceMappingRefs = new List<int>();
		}

		public string Name { get; set; }
		public IEnumerable<int> ImportSources { get; set; }
		public IEnumerable<int> SourceMappingRefs { get; set; }
	}
}