﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[Route("admin/mutableideas/import-source")]
    public class ImportSourceController : BaseCrudController<ImportSource, ImportSourceModel>
    {
		readonly IImportSourceService _importSourceService;

		public ImportSourceController(IImportSourceService service) : base(service)
		{
			_importSourceService = service;
		}

		[HttpGet("parent/{parentId:int}")]
		public IActionResult GetByParent(int parentId)
		{
			IEnumerable<ImportSourceModel> models = _importSourceService.GetForCollection(parentId);
			return Ok(models);
		}

		[HttpPost("save-source")]
		public async Task<IActionResult> SaveSource(ImportSourceModel sourceModel)
		{
			ImportSourceModel model = await _importSourceService.SaveAsync(sourceModel);
			return Ok(model);
		}
    }
}