﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Nop.Plugin.Catalog.Doba.Domain.Entities;
using Nop.Plugin.Catalog.Doba.Domain.Models;

namespace Nop.Plugin.Catalog.Doba.Domain.Services.Data
{
    public interface IDobaProductService : ICrudService<DobaProduct, DobaProductModel>
	{
		IEnumerable<DobaProductModel> ConvertImportModels(IEnumerable<DobaProductImportModel> importModels);
		Task Import(IEnumerable<int> ids); 
	}
}