﻿using Nop.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories
{
	public class SourceKeyColumnRepository : EfRepository<SourceKeyColumn>, ISourceKeyColumnRepository
	{
		public SourceKeyColumnRepository(IImportManagerContext context) : base(context) { }
	}
}