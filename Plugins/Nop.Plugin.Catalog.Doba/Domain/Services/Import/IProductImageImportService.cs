﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.Doba.Domain.Services.Import
{
    public interface IProductImageImportService
	{
		Task ImportImages(int productId, IEnumerable<string> productImagery);
    }
}