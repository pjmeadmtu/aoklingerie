﻿using Nop.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories
{
	public class ImportCollectionRepository : EfRepository<ImportCollection>, IImportCollectionRepository
	{
		public ImportCollectionRepository(IImportManagerContext context) : base(context) { }
	}
}