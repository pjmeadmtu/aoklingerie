﻿using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class SourceKeyColumnModel : BaseEntityModel
	{
		public int? SourceColumnId { get; set; }
		public int SourceClassMappingId { get; set; }
		public int? ReferenceClassMappingId { get; set; }
		public string Name { get; set; }
		public ImportEntityType EntityType { get; set; }
		public ColumnType ColumnType { get; set; }
	}
}
