﻿using System;

using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Widgets.DiscountModal.Domain.Services;
using Nop.Plugin.Widgets.DiscountModal.Models;
using Nop.Services.Configuration;
using Nop.Services.Discounts;
using Nop.Services.Messages;

namespace Nop.Plugin.Widgets.DiscountModal.Services
{
	public class DiscountMessageService : IDiscountMessageService
	{
		readonly IDiscountService _discountService;
		readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		readonly ISettingService _settingService;
		readonly IEmailSender _emailSender;
		readonly IEmailAccountService _emailAccountService;
		readonly ICampaignService _campaignService;
		readonly EmailAccountSettings _emailAccountSettings;

		public DiscountMessageService(
			IDiscountService discountService,
			INewsLetterSubscriptionService newsLetterSubscription,
			ISettingService settingService,
			IEmailSender emailSender,
			IEmailAccountService emailAccountService,
			ICampaignService campaignService,
			EmailAccountSettings emailAccountSettings)
		{
			_discountService = discountService;
			_newsLetterSubscriptionService = newsLetterSubscription;
			_settingService = settingService;
			_emailSender = emailSender;
			_emailAccountService = emailAccountService;
			_emailAccountSettings = emailAccountSettings;
			_campaignService = campaignService;
		}

		public void SendDiscountMessage(string emailAddress, int storeId)
		{
			SubscribeEmail(emailAddress, storeId);

			var modalSettings = _settingService.LoadSetting<DiscountModalSettings>();
			EmailAccount emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
			Campaign campaign = _campaignService.GetCampaignById(modalSettings.CampaignId);
			Discount discount = _discountService.GetDiscountById(modalSettings.DiscountId);
			campaign.Body = campaign.Body.Replace("{{COUPON_CODE}}", discount.CouponCode);

			_emailSender.SendEmail(emailAccount, campaign.Subject, campaign.Body, emailAccount.Email, emailAccount.DisplayName, emailAddress, string.Empty);
		}

		public DiscountModalSettingsModel GetDiscountModalSettingsModel(int storeId = 0)
		{
			DiscountModalSettings settings = _settingService.LoadSetting<DiscountModalSettings>(storeId);
			DiscountModalSettingsModel model = new DiscountModalSettingsModel { ShouldDisplayModal = false };

			if (settings == null)
				return model;

			Discount discount = _discountService.GetDiscountById(settings.DiscountId);
			Campaign campaign = _campaignService.GetCampaignById(settings.CampaignId);

			model.ButtonText = settings.ButtonText;
			model.CampaignId = settings.CampaignId;
			model.CancelMessage = settings.CancelMessage;
			model.ClosingText = settings.ClosingText;
			model.CustomCss = settings.CustomCss;
			model.Delay = settings.Delay;
			model.DiscountId = settings.DiscountId;
			model.LastUpdate = settings.LastUpdate;
			model.Message = settings.Message;
			model.PlaceHolderText = settings.PlaceHolderText;
			model.ThanksForSubscribing = settings.ThanksForSubscribing;
			model.Title = settings.Title;

			// if the discount is not null check that the discount is still running
			if (discount != null && campaign != null)
				model.ShouldDisplayModal = discount.StartDateUtc < DateTime.UtcNow && (discount.EndDateUtc.HasValue == false || discount.EndDateUtc.Value > DateTime.UtcNow);

			return model;
		}

		public bool HasEmailSubscribed(string email, int storeId)
		{
			NewsLetterSubscription subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(email, storeId);
			return subscription != null;
		}

		void SubscribeEmail(string email, int storeId)
		{
			var subscription = new NewsLetterSubscription
			{
				Active = true,
				CreatedOnUtc = DateTime.UtcNow,
				Email = email,
				StoreId = storeId,
				NewsLetterSubscriptionGuid = Guid.NewGuid()
			};

			_newsLetterSubscriptionService.InsertNewsLetterSubscription(subscription, true);
		}
	}
}