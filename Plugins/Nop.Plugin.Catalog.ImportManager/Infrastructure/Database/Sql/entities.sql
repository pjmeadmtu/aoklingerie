IF NOT EXISTS(
	SELECT t.*
	FROM sys.tables as t
	INNER JOIN sys.objects AS obj ON t.object_id = obj.object_id
	INNER JOIN sys.schemas AS sch ON obj.schema_id = obj.schema_id
	WHERE sch.name = 'dbo' AND t.name = 'MI_ImportMgr_ImportCollection'
) BEGIN

	CREATE TABLE [dbo].[MI_ImportMgr_ImportCollection](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	 CONSTRAINT [PK_MI_ImportMgr_ImportCollection] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_ImportSource](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[ImportCollectionId] [int] NULL,
		[Name] [nvarchar](100) NOT NULL,
		[SourceType] [int] NOT NULL,
		[IsFirstRowHeader] [bit] NOT NULL,
		[Source] [varbinary](max) NULL,
		[SubSourceType] [int] NULL,
		[Url] [nvarchar](2083) NULL,
		[LastImport] [datetime] NULL,
	 CONSTRAINT [PK_MI_ImportMgr_ImportSource] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


	CREATE TABLE [dbo].[MI_ImportMgr_ImportSourceColumn](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceId] [int] NOT NULL,
		[Name] [nvarchar](100) NOT NULL,
		[Index] [int] NOT NULL,
	 CONSTRAINT [PK_MI_ImportMgr_ImportSourceColumn] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_InvalidSourceValue](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceId] [int] NOT NULL,
		[Value] [nvarchar](100) NOT NULL,
		[IsRegex] [bit] NULL,
	 CONSTRAINT [PK_MI_ImportMgr_SourceInvalidValues] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_SourceClassMapping](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceId] [int] NOT NULL,
		[Name] [nvarchar](100) NOT NULL,
		[EntityType] [int] NOT NULL,
	 CONSTRAINT [PK_MI_ImportMgr_SourceClassMapping] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_SourceKeyColumn](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceColumnId] [int] NULL,
		[SourceClassMappingId] [int] NOT NULL,
		[ReferenceClassMappingId] [int] NULL,
		[Name] [nvarchar](100) NOT NULL,
		[ColumnType] [int] NOT NULL,
	 CONSTRAINT [PK_MI_ImportMgr_SourceColumnKey] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_SourceMapping](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceClassMappingId] [int] NOT NULL,
		[SourceColumnId] [int] NULL,
		[ReferenceClassMappingId] [int] NULL,
		[ColumnType] [int] NOT NULL,
		[DefaultValue] [nvarchar](2000) NULL,
		[DestinationPropertyName] [nvarchar](100) NULL,
		[Overwrite] [bit] NULL,
	 CONSTRAINT [PK_MI_ImportMgr_SourceMapping] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE TABLE [dbo].[MI_ImportMgr_SourceMappingRef](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[SourceId] [int] NOT NULL,
		[EntityId] [int] NOT NULL,
		[SourceIdValue] [nvarchar](2000) NOT NULL,
		[EntityType] [int] NOT NULL,
	 CONSTRAINT [PK_MI_ImportMgr_SourceMappingRef] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[MI_ImportMgr_ImportSource]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_ImportSource_MI_ImportMgr_ImportCollection] FOREIGN KEY([ImportCollectionId])
	REFERENCES [dbo].[MI_ImportMgr_ImportCollection] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_ImportSource] CHECK CONSTRAINT [FK_MI_ImportMgr_ImportSource_MI_ImportMgr_ImportCollection]

	ALTER TABLE [dbo].[MI_ImportMgr_InvalidSourceValue]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceInvalidValues_MI_ImportMgr_ImportSource] FOREIGN KEY([SourceId])
	REFERENCES [dbo].[MI_ImportMgr_ImportSource] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_InvalidSourceValue] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceInvalidValues_MI_ImportMgr_ImportSource]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceClassMapping]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceClassMapping_MI_ImportMgr_ImportSource] FOREIGN KEY([SourceId])
	REFERENCES [dbo].[MI_ImportMgr_ImportSource] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceClassMapping] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceClassMapping_MI_ImportMgr_ImportSource]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_ImportSourceColumn] FOREIGN KEY([SourceColumnId])
	REFERENCES [dbo].[MI_ImportMgr_ImportSourceColumn] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_ImportSourceColumn]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_SourceClassMapping] FOREIGN KEY([SourceClassMappingId])
	REFERENCES [dbo].[MI_ImportMgr_SourceClassMapping] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_SourceClassMapping]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_SourceClassMapping1] FOREIGN KEY([ReferenceClassMappingId])
	REFERENCES [dbo].[MI_ImportMgr_SourceClassMapping] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceKeyColumn] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceKeyColumn_MI_ImportMgr_SourceClassMapping1]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_ImportSourceColumn] FOREIGN KEY([SourceColumnId])
	REFERENCES [dbo].[MI_ImportMgr_ImportSourceColumn] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_ImportSourceColumn]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_SourceClassMapping] FOREIGN KEY([SourceClassMappingId])
	REFERENCES [dbo].[MI_ImportMgr_SourceClassMapping] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_SourceClassMapping]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_SourceClassMapping1] FOREIGN KEY([ReferenceClassMappingId])
	REFERENCES [dbo].[MI_ImportMgr_SourceClassMapping] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMapping] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceMapping_MI_ImportMgr_SourceClassMapping1]

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMappingRef]  WITH CHECK ADD  CONSTRAINT [FK_MI_ImportMgr_SourceMappingRef_MI_ImportMgr_ImportSource] FOREIGN KEY([SourceId])
	REFERENCES [dbo].[MI_ImportMgr_ImportSource] ([Id])

	ALTER TABLE [dbo].[MI_ImportMgr_SourceMappingRef] CHECK CONSTRAINT [FK_MI_ImportMgr_SourceMappingRef_MI_ImportMgr_ImportSource]

END