﻿using System;
using System.Linq.Expressions;
using AutoMapper;

namespace Nop.Plugin.Catalog.Doba.Extensions
{
	public static class IMappingExpressionExtensions
	{
		public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
			this IMappingExpression<TSource, TDestination> map,
			Expression<Func<TDestination, object>> selector)
		{
			map.ForMember(selector, config => config.Ignore());
			return map;
		}

		public static IMappingExpression<TSource, TDestination> Map<TSource, TDestination>(
			this IMappingExpression<TSource, TDestination> map,
			Expression<Func<TSource, object>> srcSelector,
			Expression<Func<TDestination, object>> selector)
		{
			map.ForMember(selector, config => config.MapFrom(srcSelector));
			return map;
		}
	}
}
