﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nop.Plugin.Widgets.DiscountModal.Models
{
	public class ConfigurationModel
	{
		public string Title { get; set; }
		public string ButtonText { get; set; }
		public string CancelMessage { get; set; }
		public string PlaceHolderText { get; set; }
		public string Message { get; set; }
		public string ClosingText { get; set; }
		public string CustomCss { get; set; }
		public string ThanksForSubscribing { get; set; }
		public int DiscountId { get; set; }
		public int Delay { get; set; }
		public int CampaignId { get; set; }
		public IEnumerable<SelectListItem> DiscountList { get; set; }
		public IEnumerable<SelectListItem> Campaigns { get; set; }
	}
}