﻿using Nop.Core.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories
{
	public interface ISourceMappingRepository : IRepository<SourceMapping> { }
}