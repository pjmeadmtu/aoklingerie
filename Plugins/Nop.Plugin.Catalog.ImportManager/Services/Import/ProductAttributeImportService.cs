﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductAttributeImportService : BaseImportService<ProductAttribute>, IProductAttributeImportService
	{
		public ProductAttributeImportService(IExcelExtractService excelExtractService, ISourceMappingRefService sourceMappingRefService, IProductAttributeService productAttributeService)
			: base(excelExtractService, sourceMappingRefService, ImportEntityType.ProductAttribute, productAttributeService.InsertProductAttribute, productAttributeService.UpdateProductAttribute) { }
	}
}