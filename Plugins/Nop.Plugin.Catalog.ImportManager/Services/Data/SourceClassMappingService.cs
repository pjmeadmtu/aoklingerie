﻿using System.Collections.Generic;

using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class SourceClassMappingService : BaseDataService<SourceClassMapping, SourceClassMappingModel>, ISourceClassMappingService
	{
		public SourceClassMappingService(ISourceClassMappingRepository repository, IQueryStringExpressionService<SourceClassMapping> qsExpressionService, IMapper mapper)
			: base(repository, qsExpressionService, mapper) { }

		public IEnumerable<SourceClassMappingModel> GetAllForCollection(int collectionId)
		{
			return Filter(p => p.ImportSource.ImportCollectionId == collectionId);
		}
	}
}