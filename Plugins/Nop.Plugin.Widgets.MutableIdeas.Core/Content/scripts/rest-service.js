﻿function executeRequest(baseUrl) {
	var _interceptCallback = null;

	var _executeRequest = function (options) {
		var requestOptions = {
			url: options.url,
			type: options.type,
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(options.data)
		};

		requestOptions = _interceptCallback ? _interceptCallback(requestOptions) : requestOptions;

		return $.ajax(requestOptions);
	};

	var _getBaseUrl = function () {
		return $.isFunction(baseUrl) ? baseUrl() : baseUrl;
	};

	var _createUrl = function (url) {
		var baseUrl = _getBaseUrl();

		if (isEmpty(url))
			return baseUrl;

		if (startsWith(url, '?'))
			return [baseUrl, url].join('');

		return [baseUrl, url].join('/');
	};

	var post = function (data, url) {
		return _executeRequest({ url: _createUrl(url), data: data, type: 'post' });
	};

	var put = function (data, url) {
		return _executeRequest({ url: _createUrl(url), data: data, type: 'put' });
	};

	var save = function (data) {
		return _executeRequest({ url: _createUrl('save'), data: data, type: 'post' });
	};

	var get = function (url) {
		return _executeRequest({ url: _createUrl(url), type: 'get' });
	};

	var remove = function (url, data) {
		return _executeRequest({ url: _createUrl(url), data: data, type: 'delete' });
	};

	var removeMany = function (data) {
		return _executeRequest({ url: _createUrl('many'), data: data, type: 'delete' });
	}

	return {
		get: get,
		post: post,
		update: put,
		remove: remove,
		save: save,
		removeMany: removeMany,
		intercept: function (callback) {
			_interceptCallback = callback;
		}
	};
}