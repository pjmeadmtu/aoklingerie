﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class ImportCollectionMap : NopEntityTypeConfiguration<ImportCollection>
	{
		public ImportCollectionMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}ImportCollection");
			HasKey(p => p.Id);

			HasMany(p => p.ImportSources)
				.WithRequired(p => p.ImportCollection)
				.HasForeignKey(p => p.ImportCollectionId)
				.WillCascadeOnDelete(true);

			Property(p => p.Name).HasMaxLength(100).IsRequired();
		}
	}
}