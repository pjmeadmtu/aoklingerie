﻿using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class ImportCollectionService : BaseDataService<ImportCollection, ImportCollectionModel>, IImportCollectionService
	{
		public ImportCollectionService(IImportCollectionRepository repository, IQueryStringExpressionService<ImportCollection> qsExpressionService, IMapper mapper)
			: base(repository, qsExpressionService, mapper) { }
	}
}