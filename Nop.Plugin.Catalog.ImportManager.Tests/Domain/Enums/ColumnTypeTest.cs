﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Extensions;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Tests.Domain.Enums
{
	[TestClass]
	public class ColumnTypeTest
	{
		[TestMethod]
		public void TestIsIdentifier()
		{
			IEnumerable<ColumnType> columnTypes = new[] {
				ColumnType.RowValueIdentifier,
				ColumnType.CompositeKeyValue,
				ColumnType.CompositeKeyReferenceValue
			};

			columnTypes.Iterate(column => ColumnType.IsIdentifier.Includes(column).Should().Be(true));

			ColumnType.IsIdentifier.Includes(ColumnType.RowValue).Should().Be(false);
		}

		[TestMethod]
		public void TestCompositeKeys()
		{
			IEnumerable<ColumnType> columnTypes = new[] {
				ColumnType.CompositeKeyValue,
				ColumnType.CompositeKeyReferenceValue
			};

			columnTypes.Iterate(column => ColumnType.IsCompositeKey.Includes(column).Should().Be(true));

			ColumnType.IsCompositeKey.Includes(ColumnType.RowValue).Should().Be(false);
		}
	}
}