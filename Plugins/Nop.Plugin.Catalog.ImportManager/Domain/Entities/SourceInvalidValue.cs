﻿using Nop.Core;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class SourceInvalidValue : BaseEntity
	{
		public int SourceId { get; set; }
		public string Value { get; set; }
		public bool IsRegex { get; set; }

		public virtual ImportSource ImportSource { get; set; }
	}
}