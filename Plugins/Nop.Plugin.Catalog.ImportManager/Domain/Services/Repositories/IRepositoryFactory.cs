﻿using Nop.Core;
using Nop.Core.Data;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories
{
	public interface IRepositoryFactory
	{
		IRepository<T> GetRepository<T>()
			where T : BaseEntity;
	}
}
