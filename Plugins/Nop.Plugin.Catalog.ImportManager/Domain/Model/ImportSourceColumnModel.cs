﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class ImportSourceColumnModel : BaseEntityModel
	{
		public int ImportSourceId { get; set; }
		public string Name { get; set; }
		public int Index { get; set; }
	}
}