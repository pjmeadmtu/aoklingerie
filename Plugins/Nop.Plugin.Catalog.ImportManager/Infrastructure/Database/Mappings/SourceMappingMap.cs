﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class SourceMappingMap : NopEntityTypeConfiguration<SourceMapping>
	{
		public SourceMappingMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}SourceMapping");

			HasKey(p => p.Id);

			HasRequired(p => p.SourceClassMapping)
				.WithMany(p => p.SourceMappings)
				.HasForeignKey(p => p.SourceClassMappingId);

			HasOptional(p => p.ImportSourceColumn)
				.WithMany(p => p.SourceMappings)
				.HasForeignKey(p => p.SourceColumnId);

			HasOptional(p => p.ReferenceClassMapping)
				.WithMany()
				.HasForeignKey(p => p.ReferenceClassMappingId)
				.WillCascadeOnDelete(false);

			Property(p => p.ColumnType).IsRequired();
			Property(p => p.DestinationPropertyName).HasMaxLength(100).IsRequired();
			Property(p => p.Overwrite).IsRequired();

			Property(p => p.DefaultValue).HasMaxLength(2000).IsOptional();
		}
	}
}