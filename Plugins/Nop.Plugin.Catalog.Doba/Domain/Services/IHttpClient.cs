﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.Doba.Domain.Services
{
    public interface IHttpClient
    {
		Task<HttpResponseMessage> GetAsync(string url);
		Task<HttpResponseMessage> PostAsync<T>(string url, T model);
		Task<HttpResponseMessage> PutAsync<T>(string url, T model);
		Task<HttpResponseMessage> DeleteAsync(string url);
    }
}