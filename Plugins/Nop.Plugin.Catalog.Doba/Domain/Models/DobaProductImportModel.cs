﻿using System;
using System.Xml.Serialization;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.Doba.Helpers;

namespace Nop.Plugin.Catalog.Doba.Domain.Models
{
	public class DobaProductImportModel
	{
		[XmlElement("supplier_id")]
		public string SupplierId { get; set; }

		[XmlElement("drop_ship_fee")]
		public string DropShipFeeField { get; set; }

		public decimal? DropShipFee
		{
			get { return ConversionHelper.Parse(DropShipFeeField, decimal.Parse); }
		}

		[XmlElement("supplier_name")]
		public string SupplierName { get; set; }

		[XmlElement("product_id")]
		public string ProductId { get; set; }

		[XmlElement("title")]
		public string Title { get; set; }

		[XmlElement("warranty")]
		public string Warranty { get; set; }

		[XmlElement("description")]
		public string Description { get; set; }

		[XmlElement("condition")]
		public string Condition { get; set; } // maybe an enum?

		[XmlElement("details")]
		public string Details { get; set; }

		[XmlElement("manufacturer")]
		public string Manufacturer { get; set; }

		[XmlElement("brand_name")]
		public string BrandName { get; set; }

		[XmlElement("case_pack_quantity")]
		public string CasePackQuantityField { get; set; }

		public int? CasePackQuantity =>
			ConversionHelper.Parse(CasePackQuantityField, int.Parse);

		[XmlElement("country_of_origin")]
		public string CountryOfOrigin { get; set; }

		[XmlElement("product_last_update")]
		public string ProductLastUpdatedXml { get; set; }

		public DateTime? ProductLastUpdated =>
			ConversionHelper.Parse(ProductLastUpdatedXml, DateTime.Parse);

		[XmlElement("item_id")]
		public string ItemId { get; set; }

		[XmlElement("item_sku")]
		public string Sku { get; set; }

		[XmlElement("mpn")]
		public string Mpm { get; set; }

		[XmlElement("upc")]
		public string Upc { get; set; }

		[XmlElement("item_name")]
		public string ItemName { get; set; }

		[XmlElement("item_weight")]
		public string ItemWeightField { get; set; }

		public decimal? ItemWeight =>
			ConversionHelper.Parse(ItemWeightField, decimal.Parse);

		[XmlElement("ship_alone")]
		public string ShipAloneField { get; set; } // not sure if this is a boolean field

		public int? ShipAlone =>
			ConversionHelper.Parse(ShipAloneField, int.Parse);

		[XmlElement("ship_freight")]
		public string ShipFreightField { get; set; } // not quite sure what this field represents

		public decimal? ShipFreight =>
			ConversionHelper.Parse(ShipFreightField, decimal.Parse);

		[XmlElement("ship_weight")]
		public string ShipWeightField { get; set; }

		public decimal? ShipWeight =>
			ConversionHelper.Parse(ShipWeightField, decimal.Parse);

		[XmlElement("ship_cost")]
		public string ShipCostField { get; set; }

		public decimal? ShipCost =>
			ConversionHelper.Parse(ShipCostField, decimal.Parse);

		[XmlElement("max_ship_single_box")]
		public string MaxShipSingleBox { get; set; } // not sure what this one means

		[XmlElement("map")]
		public string MapField { get; set; }

		public decimal? Map =>
			ConversionHelper.Parse(MapField, decimal.Parse);

		[XmlElement("price")]
		public string PriceField { get; set; }

		public decimal? Price =>
			ConversionHelper.Parse(PriceField, decimal.Parse);

		[XmlElement("prepay_price")]
		public string PrepayPriceField { get; set; }

		public decimal? PrepayPrice =>
			ConversionHelper.Parse(PrepayPriceField, decimal.Parse);

		[XmlElement("min_street_price")]
		public string MinStreetPriceField { get; set; }

		public decimal? MinStreetPrice =>
			ConversionHelper.Parse(MinStreetPriceField, decimal.Parse);

		[XmlElement("msrp")]
		public string MsrpField { get; set; }

		public decimal? Msrp =>
			ConversionHelper.Parse(MsrpField, decimal.Parse);

		[XmlElement("qty_avail")]
		public string QuantityAvailableField { get; set; }

		public int? QuantityAvailable =>
			ConversionHelper.Parse(QuantityAvailableField, int.Parse);

		[XmlElement("stock")]
		public string InStock { get; set; } // what are the available values for this item?

		[XmlElement("est_avail")]
		public string EstAvailField { get; set; }

		public DateTime? EstAvail =>
			ConversionHelper.Parse(EstAvailField, DateTime.Parse);

		[XmlElement("pending_order_quantity")]
		public string PendingOrderQuantityField { get; set; }

		public int? PendingOrderQuantity =>
			ConversionHelper.Parse(PendingOrderQuantityField, int.Parse);

		[XmlElement("qty_on_order")]
		public string QtyOnOrderFeild { get; set; }

		public int? QtOnOrderFeild =>
			ConversionHelper.Parse(QtyOnOrderFeild, int.Parse);

		[XmlElement("item_last_update")]
		public string ItemLastUpdatedField { get; set; }

		public DateTime? ItemLastUpdated =>
			ConversionHelper.Parse(ItemLastUpdatedField, DateTime.Parse);

		[XmlElement("item_discontinued_date")]
		public string ItemDiscountedDateField { get; set; }

		public DateTime? ItemDiscountedDate =>
			ConversionHelper.Parse(ItemDiscountedDateField, DateTime.Parse);

		[XmlElement("categories")]
		public string Categories { get; set; }

		[XmlElement("attributes")]
		public string Attributes { get; set; }

		[XmlElement("image_file")]
		public string ImageFile { get; set; }

		[XmlElement("image_width")]
		public int ImageWidth { get; set; }

		[XmlElement("image_height")]
		public int ImageHeight { get; set; }

		[XmlElement("additional_images")]
		public string AdditionalImages { get; set; } // not sure if this will have other nodes in it

		[XmlElement("folder_paths")]
		public string FolderPaths { get; set; }

		[XmlElement("is_customized")]
		public int IsCustomized { get; set; } // 0 or 1 I assume

		// These are properties that should be filled by the front end user
		public bool AvailableForPreOrder { get; set; }
		public BackorderMode BackorderMode { get; set; }
		public bool CustomerEntersPrice { get; set; }
		public bool IsFreeShipping { get; set; }
    }
}