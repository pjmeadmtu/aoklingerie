﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nop.Plugin.Catalog.Doba.Domain.Services;

namespace Nop.Plugin.Catalog.Doba.Services
{
	public class HttpClientWrapper : IHttpClient
	{

		Task<HttpResponseMessage> RunInContext(Func<HttpClient, Task<HttpResponseMessage>> request)
		{
			using (HttpClient client = new HttpClient())
			{
				return request(client);
			}
		}

		Task<HttpResponseMessage> PushModel<T>(string url, T model, Func<string, HttpContent, Task<HttpResponseMessage>> pushFunc)
		{
			using (HttpContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"))
			{
				return pushFunc(url, content);
			}
		}

		public async Task<HttpResponseMessage> GetAsync(string url)
		{
			using (HttpClient client = new HttpClient())
			{
				return await client.GetAsync(url);
			}
		}

		public async Task<HttpResponseMessage> PostAsync<T>(string url, T model)
		{
			return await RunInContext(httpClient =>
			{
				return PushModel(url, model, httpClient.PostAsync);
			});
		}

		public async Task<HttpResponseMessage> PutAsync<T>(string url, T model)
		{
			return await RunInContext(httpClient =>
			{
				return PushModel(url, model, httpClient.PutAsync);
			});
		}

		public async Task<HttpResponseMessage> DeleteAsync(string url)
		{
			return await RunInContext(httpClient =>
			{
				return httpClient.DeleteAsync(url);
			});
		}
	}
}