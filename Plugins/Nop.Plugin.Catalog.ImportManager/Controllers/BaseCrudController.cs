﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Extensions;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	public abstract class BaseCrudController<T, TModel> : BasePluginController
		where T : BaseEntity
		where TModel : BaseEntityModel
	{
		readonly ICrudService<T, TModel> _crudService;
		PagingOptions _pagingOptions = null;

		PagingOptions PagingOptions
		{
			get
			{
				_pagingOptions = _pagingOptions ?? Request.Query.GetPagingOptions();
				return _pagingOptions;
			}
		}

		protected BaseCrudController(ICrudService<T, TModel> crudService)
		{
			_crudService = crudService;
		}

		[HttpGet]
		public virtual IActionResult Get()
		{
			if (PagingOptions.HasPaging)
			{
				PageModel<TModel> pageModel = _crudService.GetPagedModel(PagingOptions);
				return Ok(pageModel);
			}

			if (!string.IsNullOrEmpty(PagingOptions.Filter))
			{
				IEnumerable<TModel> filteredModels = _crudService.Filter(PagingOptions.Filter);
				return Ok(filteredModels.ToArray());
			}

			IEnumerable<TModel> models = _crudService.Load();
			return Ok(models.ToArray());
		}

		[HttpGet("{id}")]
		public virtual IActionResult Get(int id)
		{
			TModel model = _crudService.GetById(id);
			return Ok(model);
		}

		[HttpPost]
		public virtual IActionResult Post([FromBody]TModel model)
		{
			model = _crudService.Insert(model);
			return Ok(model);
		}

		[HttpPut]
		public virtual IActionResult Put([FromBody]TModel model)
		{
			_crudService.Update(model);
			return Accepted(model);
		}

		[HttpPost("Save")]
		public virtual IActionResult Save([FromBody]TModel model)
		{
			return model.Id.HasValue ? Put(model) : Post(model);
		}

		[HttpDelete("{id}")]
		public virtual IActionResult Delete(int id)
		{
			_crudService.Delete(id);
			return NoContent();
		}

		[HttpDelete("many")]
		public virtual IActionResult DeleteMany([FromBody]IEnumerable<int> selectedIds)
		{
			_crudService.Delete(selectedIds);
			return Ok();
		}
    }
}
