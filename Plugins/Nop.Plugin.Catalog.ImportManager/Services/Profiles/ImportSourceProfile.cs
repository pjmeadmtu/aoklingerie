﻿using System.Linq;

using AutoMapper;

using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class ImportSourceProfile : Profile
	{
		public ImportSourceProfile()
		{
			CreateMap<ImportSource, ImportSourceModel>()
				.Map(p => p.SourceClassMappings.Select(m => m.Id).ToArray(), p => p.SourceClassMappings)
				.Map(p => p.ImportSourceColumns.Select(m => m.Id).ToArray(), p => p.ImportSourceColumns)
				.Map(p => p.InvalidValues.Select(m => m.Id).ToArray(), p => p.InvalidValues)
				.Ignore(p => p.FileUpload);

			CreateMap<ImportSourceModel, ImportSource>()
				.Map(p => p.Id.HasValue ? p.Id.Value : -1, p => p.Id)
				.Ignore(p => p.ImportCollection)
				.Ignore(p => p.SourceClassMappings)
				.Ignore(p => p.ImportSourceColumns)
				.Ignore(p => p.InvalidValues)
				.Ignore(p => p.Source);
		}
	}
}