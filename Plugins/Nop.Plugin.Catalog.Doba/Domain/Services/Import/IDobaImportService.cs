﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Nop.Plugin.Catalog.Doba.Domain.Models;

namespace Nop.Plugin.Catalog.Doba.Domain.Services.Import
{
    public interface IDobaImportService
    {
		Task<IEnumerable<DobaProductModel>> ImportList(Stream stream);
    }
}