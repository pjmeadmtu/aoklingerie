﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;
using System.Dynamic;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "SourceTypeDropdown")]
	public class SourceTypeDropdownViewComponent : EnumDropdownViewComponent<SourceType> { }
}
