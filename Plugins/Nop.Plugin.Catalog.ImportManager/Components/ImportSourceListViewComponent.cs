﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "ImportSourceList")]
	public class ImportSourceListViewComponent : NopViewComponent
	{
		public IViewComponentResult Invoke(int collectionId)
		{
			return View($"{SharedPath}/ImportSourceList.cshtml", collectionId);
		}
	}
}