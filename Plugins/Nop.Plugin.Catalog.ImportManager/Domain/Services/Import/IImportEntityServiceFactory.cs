﻿using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Import
{
	public interface IImportEntityServiceFactory
	{
		IEntityImportService GetImportService(ImportEntityType entityType);
	}
}