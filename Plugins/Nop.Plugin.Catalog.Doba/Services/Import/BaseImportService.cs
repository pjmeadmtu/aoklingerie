﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using AutoMapper;

using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;

namespace Nop.Plugin.Catalog.Doba.Services.Import
{
    public abstract class BaseImportService : IDobaImportService
	{
		readonly IMapper _mapper;
		readonly IDobaProductService _dobaProductService;

		protected BaseImportService(IMapper mapper, IDobaProductService dobaProductService)
		{
			_mapper = mapper;
			_dobaProductService = dobaProductService;
		}

		protected abstract Task<IEnumerable<DobaProductImportModel>> ParseList(Stream stream);

		public virtual async Task<IEnumerable<DobaProductModel>> ImportList(Stream stream)
		{
			IEnumerable<DobaProductImportModel> dobaImportProductModels = await ParseList(stream);
			IEnumerable<DobaProductModel> dobaProductModels = _dobaProductService.ConvertImportModels(dobaImportProductModels);
			dobaProductModels = _dobaProductService.Save(dobaProductModels);

			return dobaProductModels;
		}
	}
}