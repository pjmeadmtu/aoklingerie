﻿using System.Linq;

using Nop.Core;
using Nop.Core.Plugins;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Services.Common;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Catalog.Doba
{
	public class ImportManagerPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
	{
		readonly IWebHelper _webHelper;
		readonly IImportManagerContext _importManagerContext;

		public ImportManagerPlugin(IWebHelper webHelper, IImportManagerContext importManagerContext)
		{
			_webHelper = webHelper;
			_importManagerContext = importManagerContext;
		}

		public void ManageSiteMap(SiteMapNode rootNode)
		{
			SiteMapNode siteMapNode = rootNode.ChildNodes.FirstOrDefault(node => node.SystemName == "Catalog");

			if (siteMapNode == null) return;

			SiteMapNode dobaNode = new SiteMapNode
			{
				SystemName = "MutableIdeas",
				Title = "Mutable Ideas",
				Visible = true,
				IconClass = "fa-dot-circle-o"
			};

			SiteMapNode dobaImport = new SiteMapNode
			{
				SystemName = "Plugin.Catalog.ImportManager",
				Title = "Import Manager",
				Visible = true,
				IconClass = "fa-dot-circle-o",
				Url = "/admin/mutableideas/import"
			};

			dobaNode.ChildNodes.Add(dobaImport);
			siteMapNode.ChildNodes.Add(dobaNode);
		}

		public override void Install()
		{
			_importManagerContext.Install();
			base.Install();
		}

		public override void Uninstall()
		{
			_importManagerContext.Uninstall();
			base.Uninstall();
		}
	}
}