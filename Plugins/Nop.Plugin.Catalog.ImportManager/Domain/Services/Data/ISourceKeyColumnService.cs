﻿using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface ISourceKeyColumnService : ICrudService<SourceKeyColumn, SourceKeyColumnModel> { }
}