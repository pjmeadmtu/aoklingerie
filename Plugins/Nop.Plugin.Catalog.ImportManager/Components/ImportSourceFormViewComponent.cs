﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "ImportSourceForm")]
	public class ImportSourceFormViewComponent : NopViewComponent
	{
		public IViewComponentResult Invoke(int collectionId)
		{
			return View($"{SharedPath}/ImportSourceForm.cshtml", new ImportSourceModel { ImportCollectionId = collectionId });
		}
	}
}
