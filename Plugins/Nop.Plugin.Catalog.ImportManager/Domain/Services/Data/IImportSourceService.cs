﻿using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface IImportSourceService : ICrudService<ImportSource, ImportSourceModel>
	{
		IEnumerable<ImportSourceModel> GetForCollection(int collectionId);
		Task<ImportSourceModel> InsertAsync(ImportSourceModel model);
		Task<ImportSourceModel> UpdateAsync(ImportSourceModel model);
		Task<ImportSourceModel> SaveAsync(ImportSourceModel model);
		Stream GetSourceStream(int id);
	}
}