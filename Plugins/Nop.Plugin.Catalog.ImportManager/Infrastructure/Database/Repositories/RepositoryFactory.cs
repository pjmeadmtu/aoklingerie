﻿using Autofac;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Repositories
{
	public class RepositoryFactory : IRepositoryFactory
	{
		readonly IComponentContext _componentContext;

		public RepositoryFactory(IComponentContext componentContext)
		{
			_componentContext = componentContext;
		}

		public IRepository<T> GetRepository<T>() where T : BaseEntity
		{
			return _componentContext.Resolve<IRepository<T>>();
		}
	}
}