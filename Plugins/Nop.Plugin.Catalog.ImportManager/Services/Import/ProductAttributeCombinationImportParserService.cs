﻿using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductAttributeCombinationImportParserService : IProductAttributeCombinationImportParserService
	{
		readonly ISourceMappingRefService _sourceMappingRefService;
		readonly IProductAttributeParser _productAttributeParser;
		readonly IRepository<ProductAttributeMapping> _attributeMappingRepository;
		readonly IRepository<ProductAttributeValue> _attributeValueRepository;

		public ProductAttributeCombinationImportParserService(
			ISourceMappingRefService sourceMappingRefService,
			IProductAttributeParser productAttributeParser,
			IRepository<ProductAttributeMapping> attributeMappingRepository,
			IRepository<ProductAttributeValue> attributeValueRepository)
		{
			_sourceMappingRefService = sourceMappingRefService;
			_productAttributeParser = productAttributeParser;
			_attributeMappingRepository = attributeMappingRepository;
			_attributeValueRepository = attributeValueRepository;
		}

		public string ParseAttributes(int productId, int sourceId, IEnumerable<int> entityIds)
		{
			IEnumerable<ProductAttributeValue> attributeValues = _attributeValueRepository.Where(p => entityIds.Contains(p.Id)).ToArray();
			IEnumerable<ProductAttributeMapping> mappings = _attributeMappingRepository.Where(p => p.ProductId == productId)
				.ToArray();

			string productXml = string.Empty;
			foreach (ProductAttributeMapping mapping in mappings)
			{
				string attributeValueId = attributeValues.FirstOrDefault(p => p.ProductAttributeMappingId == mapping.Id)?.Id.ToString();

				if (attributeValueId != null)
					productXml = _productAttributeParser.AddProductAttribute(productXml, mapping, attributeValueId);
			}

			return productXml;
		}
	}
}
