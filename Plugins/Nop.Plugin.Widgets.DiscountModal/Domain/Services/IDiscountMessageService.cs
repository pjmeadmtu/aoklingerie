﻿using Nop.Plugin.Widgets.DiscountModal.Models;

namespace Nop.Plugin.Widgets.DiscountModal.Domain.Services
{
	public interface IDiscountMessageService
	{
		void SendDiscountMessage(string emailAddress, int storeId);
		bool HasEmailSubscribed(string emailAddress, int storeId);
		DiscountModalSettingsModel GetDiscountModalSettingsModel(int storeId = 0);
	}
}