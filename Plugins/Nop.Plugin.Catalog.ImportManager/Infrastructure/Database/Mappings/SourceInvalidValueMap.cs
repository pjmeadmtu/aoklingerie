﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class SourceInvalidValueMap : NopEntityTypeConfiguration<SourceInvalidValue>
	{
		public SourceInvalidValueMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}InvalidSourceValue");
			HasKey(p => p.Id);

			HasRequired(p => p.ImportSource)
				.WithMany(p => p.InvalidValues)
				.HasForeignKey(p => p.SourceId);

			Property(p => p.Value).HasMaxLength(100).IsRequired();
			Property(p => p.IsRegex).IsRequired();
		}
	}
}