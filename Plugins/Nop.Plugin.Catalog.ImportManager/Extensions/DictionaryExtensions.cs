﻿using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class DictionaryExtensions
	{
		public static void AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value)
		{
			if (dict.ContainsKey(key))
			{
				dict[key] = value;
				return;
			}

			dict.Add(key, value);
		}
	}
}
