﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	public abstract class EnumDropdownViewComponent<T> : NopViewComponent
		where T : struct
	{
		public virtual IViewComponentResult Invoke(T enumValue, string id)
		{
			Type itemType = typeof(T);

			IList<SelectListItem> listItems = ((IEnumerable<T>)Enum.GetValues(itemType))
				.Select(p =>
				{
					return new SelectListItem
					{
						Text = Enum.GetName(itemType, p).Humanize(),
						Value = Convert.ToInt32(p).ToString(),
						Selected = p.Equals(enumValue)
					};
				}).ToList();

			listItems.Insert(0, new SelectListItem
			{
				Text = "- Select -",
				Value = ""
			});

			dynamic expandoObject = new ExpandoObject();
			expandoObject.ListItems = listItems;
			expandoObject.Id = id;

			return View($"{SharedPath}/EnumTypeDropdown.cshtml", expandoObject);
		}
	}
}
