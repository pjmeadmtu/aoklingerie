﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class ImportSourceModel : BaseEntityModel
	{
		public ImportSourceModel()
		{
			SourceClassMappings = new List<int>();
			ImportSourceColumns = new List<int>();
		}

		public int ImportCollectionId { get; set; }
		public string Name { get; set; }
		public SourceType SourceType { get; set; }
		public SourceType? SubSourceType { get; set; }
		public string Url { get; set; }
		public bool IsFirstRowHeader { get; set; }
		public DateTime? LastImport { get; set; }
		public IFormFile FileUpload { get; set; }
		public IEnumerable<int> SourceClassMappings { get; set; }
		public IEnumerable<int> ImportSourceColumns { get; set; }
		public IEnumerable<int> InvalidValues { get; set; }
	}
}