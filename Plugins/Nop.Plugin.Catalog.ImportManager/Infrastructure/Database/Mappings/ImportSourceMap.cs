﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class ImportSourceMap : NopEntityTypeConfiguration<ImportSource>
	{
		public ImportSourceMap()
		{
			ToTable($"{ MUTABLE_IDEAS_PREFIX}ImportSource");
			HasKey(p => p.Id);

			HasRequired(p => p.ImportCollection)
				.WithMany(p => p.ImportSources)
				.HasForeignKey(p => p.ImportCollectionId);

			HasMany(p => p.SourceClassMappings)
				.WithRequired(p => p.ImportSource)
				.WillCascadeOnDelete(true);

			HasMany(p => p.ImportSourceColumns)
				.WithRequired(p => p.ImportSource)
				.WillCascadeOnDelete(true);

			Property(p => p.Name).HasMaxLength(100)
				.IsRequired();
			Property(p => p.SourceType).IsRequired();
			Property(p => p.IsFirstRowHeader).IsRequired();

			Property(p => p.Source).IsOptional();
			Property(p => p.LastImport).IsOptional();
			Property(p => p.SubSourceType).IsOptional();
			Property(p => p.Url).HasMaxLength(2083).IsOptional();
		}
	}
}