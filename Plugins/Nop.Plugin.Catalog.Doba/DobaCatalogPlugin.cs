﻿using System.Linq;

using Nop.Core;
using Nop.Core.Plugins;
using Nop.Plugin.Catalog.Doba.Domain.Database;
using Nop.Services.Common;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Catalog.Doba
{
	public class DobaCatalogPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
	{
		readonly IWebHelper _webHelper;
		readonly IDobaDbContext _dobaDbContext;

		public DobaCatalogPlugin(IWebHelper webHelper, IDobaDbContext dobaDbContext)
		{
			_webHelper = webHelper;
			_dobaDbContext = dobaDbContext;
		}

		public void ManageSiteMap(SiteMapNode rootNode)
		{
			SiteMapNode siteMapNode = rootNode.ChildNodes.FirstOrDefault(node => node.SystemName == "Catalog");

			if (siteMapNode == null) return;

			SiteMapNode dobaNode = new SiteMapNode
			{
				SystemName="Doba",
				Title = "Doba",
				Visible = true,
				IconClass = "fa-dot-circle-o"
			};

			SiteMapNode dobaImport = new SiteMapNode
			{
				SystemName = "Plugin.Catalog.Doba.Import",
				Title = "Import",
				Visible = true,
				IconClass = "fa-dot-circle-o",
				Url = "/admin/doba/import"
			};

			dobaNode.ChildNodes.Add(dobaImport);
			siteMapNode.ChildNodes.Add(dobaNode);
		}

		public override void Install()
		{
			_dobaDbContext.Install();
			base.Install();
		}

		public override void Uninstall()
		{
			_dobaDbContext.Uninstall();
			base.Uninstall();
		}
	}
}