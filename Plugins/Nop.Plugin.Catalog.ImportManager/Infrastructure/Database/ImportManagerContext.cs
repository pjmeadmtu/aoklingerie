﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Reflection;
using Nop.Core;
using Nop.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings;

using EFDatabase = System.Data.Entity.Database;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database
{
	public class ImportManagerContext : DbContext, IImportManagerContext
	{
		public ImportManagerContext(string nameOrConnectionString) : base(nameOrConnectionString)
		{
			Configuration.LazyLoadingEnabled = true;
			Configuration.ProxyCreationEnabled = true;
		}

		/// <summary>
		/// Gets or sets a value indicating whether proxy creation setting is enabled (used in EF)
		/// </summary>
		public virtual bool ProxyCreationEnabled
		{
			get { return Configuration.ProxyCreationEnabled; }
			set { Configuration.ProxyCreationEnabled = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether auto detect changes setting is enabled (used in EF)
		/// </summary>
		public virtual bool AutoDetectChangesEnabled
		{
			get { return Configuration.AutoDetectChangesEnabled; }
			set { Configuration.AutoDetectChangesEnabled = value; }
		}

		/// <summary>
		/// Add entity to the configuration of the model for a derived context before it is locked down
		/// </summary>
		/// <param name="modelBuilder">The builder that defines the model for the context being created</param>
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new ImportCollectionMap());
			modelBuilder.Configurations.Add(new ImportSourceMap());
			modelBuilder.Configurations.Add(new ImportSourceColumnMap());
			modelBuilder.Configurations.Add(new SourceClassMappingMap());
			modelBuilder.Configurations.Add(new SourceMappingMap());
			modelBuilder.Configurations.Add(new SourceMappingRefMap());
			modelBuilder.Configurations.Add(new SourceInvalidValueMap());
			modelBuilder.Configurations.Add(new SourceKeyColumnMap());

			base.OnModelCreating(modelBuilder);
		}

		/// <summary>
		/// Generates a data definition language script that creates schema objects
		/// </summary>
		/// <returns>A DDL script</returns>
		public string CreateDatabaseScript()
		{
			using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Sql.entities.sql"))
			using (StreamReader reader = new StreamReader(stream))
			{
				return reader.ReadToEnd();
			}
		}

		/// <summary>
		/// Returns a System.Data.Entity.DbSet`1 instance for access to entities of the given type in the context and the underlying store
		/// </summary>
		/// <typeparam name="TEntity">The type entity for which a set should be returned</typeparam>
		/// <returns>A set for the given entity type</returns>
		public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
		{
			return base.Set<TEntity>();
		}

		/// <summary>
		/// Install object context
		/// </summary>
		public void Install()
		{
			EFDatabase.SetInitializer<ImportManagerContext>(null);
			string sql = CreateDatabaseScript();
			Database.ExecuteSqlCommand(sql);
			SaveChanges();
		}

		public void Uninstall()
		{
			//drop the table
			this.DropPluginTable(this.GetTableName<SourceMapping>());
			this.DropPluginTable(this.GetTableName<SourceClassMapping>());
			this.DropPluginTable(this.GetTableName<ImportSource>());
			this.DropPluginTable(this.GetTableName<ImportCollection>());
		}

		/// <summary>
		/// Execute stores procedure and load a list of entities at the end
		/// </summary>
		/// <typeparam name="TEntity">Entity type</typeparam>
		/// <param name="commandText">Command text</param>
		/// <param name="parameters">Parameters</param>
		/// <returns>Entities</returns>
		public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : BaseEntity, new()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
		/// </summary>
		/// <typeparam name="TElement">The type of object returned by the query.</typeparam>
		/// <param name="sql">The SQL query string.</param>
		/// <param name="parameters">The parameters to apply to the SQL query string.</param>
		/// <returns>Result</returns>
		public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Executes the given DDL/DML command against the database.
		/// </summary>
		/// <param name="sql">The command string</param>
		/// <param name="doNotEnsureTransaction">false - the transaction creation is not ensured; true - the transaction creation is ensured.</param>
		/// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
		/// <param name="parameters">The parameters to apply to the command string.</param>
		/// <returns>The result returned by the database after executing the command.</returns>
		public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Detach an entity
		/// </summary>
		/// <param name="entity">Entity</param>
		public void Detach(object entity)
		{
			if (entity == null)
				throw new ArgumentNullException(nameof(entity));

			((IObjectContextAdapter)this).ObjectContext.Detach(entity);
		}
	}
}
