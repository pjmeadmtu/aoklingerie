﻿using AutoMapper;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.Doba.Domain.Entities;
using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Extensions;
using System;

namespace Nop.Plugin.Catalog.Doba.Services.Profiles
{
    public class DobaProductProfile : Profile
    {
		public DobaProductProfile()
		{
			CreateMap<DobaProductImportModel, DobaProductModel>()
				.Map(p => p.DropShipFee.GetValueOrDefault(), p => p.DropshipFee)
				.Map(p => p.Msrp.GetValueOrDefault(), p => p.MSRP)
				.Map(p => p.Price.GetValueOrDefault(), p => p.Price)
				.Map(p => p.Msrp.GetValueOrDefault(), p => p.MSRP)
				.Map(p => p.QuantityAvailable.GetValueOrDefault(), p => p.QuantityAvailable)
				.Map(p => p.ShipCost.GetValueOrDefault(), p => p.ShipCost)
				.Map(p => p.ShipWeight.GetValueOrDefault(), p => p.ShipWeight)
				.Map(p => p.PrepayPrice.GetValueOrDefault(), p => p.PrepayPrice)
				.Map(p => p.SupplierName, p => p.SupplierName)
				.Ignore(p => p.IsNew)
				.Ignore(p => p.ImportProduct);

			CreateMap<DobaProductModel, DobaProduct>()
				.Map(p => (p.Id.HasValue ? p.Id.Value : -1), p => p.Id);

			CreateMap<DobaProduct, DobaProductModel>()
				.Map(p => p.Id, p => p.Id);

			CreateMap<DobaProduct, Product>()
				.BeforeMap((dobaProduct, product) => {
					product.AdminComment = "Imported with Doba Product Import.";
					product.BackorderMode = BackorderMode.AllowQtyBelow0AndNotifyCustomer;
					product.CreatedOnUtc = dobaProduct.DateCreated;
					product.DisplayStockQuantity = true;
					product.ProductCost = dobaProduct.Price + dobaProduct.DropshipFee + dobaProduct.ShipCost;
					product.UpdatedOnUtc = DateTime.UtcNow;
					product.ManageInventoryMethod = ManageInventoryMethod.ManageStock;
				})
				.Map(p => p.Description, p => p.FullDescription)
				.Map(p => p.Title, p => p.Name)
				.Map(p => p.Sku, p => p.Sku)
				.Map(p => p.ShipWeight, p => p.Weight)
				.Map(p => p.QuantityAvailable, p => p.StockQuantity)
				.Map(p => !p.EstAvail.HasValue, p => p.Published)
				.Map(p => p.MSRP, p => p.Price)
				.Map(p => p.InternalProductId.GetValueOrDefault(), p => p.Id);
		}
    }
}