﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "SourceClassMappingList")]
	public class SourceClassMappingListViewComponent : NopViewComponent
	{
		public SourceClassMappingListViewComponent() { }

		public IViewComponentResult Invoke()
		{
			return View($"{SharedPath}/SourceClassMappingList.cshtml");
		}
	}
}