﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;
using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public abstract class BaseDataService<T, TModel> : ICrudService<T, TModel>
		where T : BaseEntity
		where TModel : BaseEntityModel
	{
		readonly IRepository<T> _repository;
		readonly IMapper _mapper;
		readonly IQueryStringExpressionService<T> _queryStringExpressionService;

		protected BaseDataService(IRepository<T> repository, IQueryStringExpressionService<T> querystringExpressionService, IMapper mapper)
		{
			_queryStringExpressionService = querystringExpressionService;
			_repository = repository;
			_mapper = mapper;
		}

		public virtual void Delete(int id)
		{
			T entity = _repository.Where(p => p.Id == id).FirstOrDefault();
			_repository.Delete(entity);
		}

		public virtual void Delete(IEnumerable<int> ids)
		{
			IEnumerable<T> entities = _repository.Where(p => ids.Contains(p.Id)).ToArray();
			_repository.Delete(entities);
		}

		public virtual TModel GetById(int id)
		{
			T entity = _repository.GetById(id);
			return _mapper.Map<TModel>(entity);
		}

		public virtual PageModel<TModel> GetPagedModel(PagingOptions pageOptions)
		{
			IQueryable<T> entities = null;

			if (!string.IsNullOrEmpty(pageOptions.Filter))
			{
				Expression<Func<T, bool>> expression = _queryStringExpressionService.GetExpression(pageOptions.Filter);
				entities = Where(expression).AsQueryable();
			}

			entities = entities ?? _repository.Table.AsQueryable();

			return new PageModel<TModel>
			{
				Total = entities.Count(),
				Page = pageOptions.Page.Value,
				Models = Page(pageOptions, entities)
			};
		}

		public virtual IEnumerable<TModel> Filter(string filter)
		{
			Expression<Func<T, bool>> expression = _queryStringExpressionService.GetExpression(filter);
			return Filter(expression);
		}

		public virtual TModel Insert(TModel model)
		{
			T entity = _mapper.Map<T>(model);
			_repository.Insert(entity);

			return _mapper.Map<TModel>(entity);
		}

		public IEnumerable<TModel> Load()
		{
			return Map(() => _repository.Table).ToArray();
		}

		public virtual IEnumerable<TModel> Load(IEnumerable<int> ids)
		{
			return Filter(p => ids.Contains(p.Id));
		}

		public virtual IEnumerable<TModel> Save(IEnumerable<TModel> models)
		{
			IEnumerable<T> updatedEntities = Map(() => models.Where(p => p.Id.HasValue).ToArray()).ToArray();
			IEnumerable<T> insertEntities = Map(() => models.Where(p => !p.Id.HasValue).ToArray()).ToArray();

			_repository.Update(updatedEntities);
			_repository.Insert(insertEntities);

			insertEntities = insertEntities.Concat(updatedEntities);
			return Map(() => insertEntities);
		}

		public virtual TModel Update(TModel model)
		{
			T entity = _repository.GetById(model.Id);
			entity = _mapper.Map(model, entity);

			_repository.Update(entity);

			return model;
		}

		public virtual IEnumerable<TModel> Filter(Expression<Func<T, bool>> expression)
		{
			return Map(() => _repository.Where(expression));
		}

		protected IEnumerable<TModel> Page(PagingOptions pageOptions, IQueryable<T> entities)
		{
			IEnumerable<T> pagedEntities = !string.IsNullOrEmpty(pageOptions.OrderBy)
					? _queryStringExpressionService.Sort(pageOptions.OrderBy, entities)
					: entities;

			pagedEntities = pagedEntities.Page(pageOptions.PageSize.Value, pageOptions.Page.Value)
				.AsEnumerable();

			return Map(() => pagedEntities);
		}

		protected virtual IQueryable<T> Where(Expression<Func<T, bool>> expression)
		{
			return _repository.Where(expression).AsQueryable();
		}

		protected IEnumerable<TModel> Map(Func<IEnumerable<T>> func)
		{
			IEnumerable<T> entities = func().ToArray();
			return entities.Select(p => _mapper.Map<TModel>(p));
		}

		protected IEnumerable<T> Map(Func<IEnumerable<TModel>> func)
		{
			IEnumerable<TModel> models = func().ToArray();
			return models.Select(p => _mapper.Map<T>(p));
		}
	}

}
