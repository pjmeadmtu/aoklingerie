﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNetCore.StaticFiles;

using Nop.Core.Domain.Media;
using Nop.Plugin.Catalog.Doba.Domain.Services;
using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Services.Media;
using Nop.Services.Seo;

namespace Nop.Plugin.Catalog.Doba.Services.Import
{
	public class BaseImageImportService : IImageImportService
	{
		readonly IHttpClient _httpClient;
		readonly IPictureService _pictureService;
		readonly ITaskErrorService _taskErrorService;

		object _pictureServiceLock = new object();

		public BaseImageImportService(IHttpClient httpClient, IPictureService pictureService, ITaskErrorService taskErrorService)
		{
			_httpClient = httpClient;
			_pictureService = pictureService;
			_taskErrorService = taskErrorService;
		}

		public async virtual Task<IEnumerable<Picture>> ImportPictures(IEnumerable<string> urls)
		{
			IEnumerable<Task<Picture>> pictureTasks = urls.Select(async url => await GetProductImage(url)).ToArray();

			return await Task.Factory.ContinueWhenAll(pictureTasks.ToArray(), completedTasks =>
			{
				_taskErrorService.HandleErrors(completedTasks);
				return completedTasks.Where(p => p.Status == TaskStatus.RanToCompletion).Select(p => p.Result).ToArray();
			});
		}

		protected virtual async Task<Picture> GetProductImage(string url)
		{
			using (HttpResponseMessage response = await _httpClient.GetAsync(url))
			using (MemoryStream memStream = new MemoryStream())
			{
				await response.Content.CopyToAsync(memStream);
				return SaveProductPicture(url, memStream.ToArray());
			}
		}
		Picture SaveProductPicture(string url, byte[] imageBytes)
		{
			lock (_pictureServiceLock)
			{
				var contentTypeProvider = new FileExtensionContentTypeProvider();
				string mimeType;
				string fileName = Path.GetFileNameWithoutExtension(url);
				fileName = SeoExtensions.GetSeName(fileName, false, false);

				contentTypeProvider.TryGetContentType(url, out mimeType);

				return _pictureService.InsertPicture(imageBytes, mimeType, fileName);
			}
		}
	}
}
