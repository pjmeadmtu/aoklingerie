﻿using Nop.Data;

namespace Nop.Plugin.Catalog.Doba.Domain.Database
{
    public interface IDobaDbContext : IDbContext
	{
		void Install();
		void Uninstall();
	}
}
