﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Autofac;
using AutoMapper;

using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Catalog.Doba.Domain.Database;
using Nop.Plugin.Catalog.Doba.Domain.Repositories;
using Nop.Plugin.Catalog.Doba.Domain.Services;
using Nop.Plugin.Catalog.Doba.Infrastructure.Database;
using Nop.Plugin.Catalog.Doba.Infrastructure.Database.Repositories;
using Nop.Plugin.Catalog.Doba.Services;
using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Plugin.Catalog.Doba.Services.Import;
using Nop.Plugin.Catalog.Doba.Services.Data;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;
using Nop.Web.Framework.Infrastructure;
using Autofac.Core;

namespace Nop.Plugin.Catalog.Doba.Infrastructure
{
	public class DependencyRegistrar : IDependencyRegistrar
	{
		public int Order => 1;

		public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
		{
			var dataSettingsManager = new DataSettingsManager();
			DataSettings dataSettings = dataSettingsManager.LoadSettings();

			this.RegisterPluginDataContext<DobaDbContext>(builder, "nop_object_context_catalog_doba");

			builder.RegisterType<DobaXmlImportService>().As<IDobaImportService>().InstancePerLifetimeScope();
			builder.RegisterType<BaseImageImportService>().As<IImageImportService>().InstancePerLifetimeScope();
			builder.RegisterType<ProductPictureImportService>().As<IProductImageImportService>().InstancePerLifetimeScope();
			builder.RegisterType<HttpClientWrapper>().As<IHttpClient>().InstancePerLifetimeScope();
			builder.RegisterType<TaskErrorService>().As<ITaskErrorService>().InstancePerLifetimeScope();
			builder.RegisterType<DobaProductService>().As<IDobaProductService>().InstancePerLifetimeScope();

			builder.Register<IDobaDbContext>(c => new DobaDbContext(dataSettings.DataConnectionString)).InstancePerLifetimeScope();

			builder.RegisterType<DobaProductRepository>()
				.As<IDobaProductRepository>()
				.WithParameter(ResolvedParameter.ForNamed<IDobaDbContext>("nop_object_context_catalog_doba"))
                .InstancePerLifetimeScope();

			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			builder.RegisterAssemblyTypes(assemblies)
				.Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract && t.IsPublic)
				.As<Profile>();

			builder.Register(c => new MapperConfiguration(cfg => {
				foreach (var profile in c.Resolve<IEnumerable<Profile>>())
				{
					cfg.AddProfile(profile);
				}
			})).AsSelf().SingleInstance();

			builder.Register(c => c.Resolve<MapperConfiguration>()
				.CreateMapper(c.Resolve))
				.As<IMapper>()
				.InstancePerLifetimeScope();
		}
	}
}