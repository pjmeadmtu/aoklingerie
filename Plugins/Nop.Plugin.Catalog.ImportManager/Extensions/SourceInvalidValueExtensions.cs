﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class SourceInvalidValueExtensions
	{
		public static bool IsInvalidValue(this IEnumerable<SourceInvalidValue> invalidValues, string value)
		{
			return invalidValues.Any(p => p.Value.Trim().Equals(value.Trim(), StringComparison.OrdinalIgnoreCase)
				|| (p.IsRegex && Regex.IsMatch(value, p.Value)));
		}
	}
}