﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductAttributeValueImportService : BaseImportService<ProductAttributeValue>, IProductAttributeValueImportService
	{
		public ProductAttributeValueImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			IProductAttributeService productAttributeService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.ProductAttributeValue,
				  productAttributeService.InsertProductAttributeValue,
				  productAttributeService.UpdateProductAttributeValue) { }
		
	}
}