﻿using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class SourceKeyColumnProfile : Profile
	{
		public SourceKeyColumnProfile()
		{
			CreateMap<SourceKeyColumn, SourceKeyColumnModel>();
			CreateMap<SourceKeyColumnModel, SourceKeyColumn>()
				.Ignore(p => p.ReferenceClassMapping)
				.Ignore(p => p.SourceClassMapping);
		}
	}
}