﻿using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class SourceMappingModel : BaseEntityModel
	{
		public int SourceClassMappingId { get; set; }
		public int? SourceColumnId { get; set; }
		public int? ReferenceClassMappingId { get; set; }
		public ColumnType ColumnType { get; set; }

		public string DefaultValue { get; set; }
		public string DestinationPropertyName { get; set; }
		public bool Overwrite { get; set; }
	}
}