﻿using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts; 

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[AuthorizeAdmin]
	[Area(AreaNames.Admin)]
	[Route("admin/mutableideas/import")]
	public class ImportManagerController : BasePluginController
	{
		readonly IImportCollectionService _importCollectionService;
		readonly IImportSourceService _importSourceService;
		readonly ISourceMappingService _sourceMappingService;

		public ImportManagerController(IImportCollectionService importCollectionService, IImportSourceService importSourceService, ISourceMappingService sourceMappingService)
		{
			_importCollectionService = importCollectionService;
			_importSourceService = importSourceService;
			_sourceMappingService = sourceMappingService;
		}

		[HttpGet]
		public IActionResult Index()
		{
			if (!_importCollectionService.Load().Any())
				return RedirectToAction("Create");

			return View($"{ViewsPath}/List.cshtml");
		}

		[HttpGet("create")]
		public IActionResult Create()
		{
			return View($"{ViewsPath}/CreateEdit.cshtml", new ImportCollectionModel());
		}

		[HttpGet("{id}")]
		public IActionResult Edit(int id)
		{
			ImportCollectionModel model = _importCollectionService.GetById(id);
			return View($"{ViewsPath}/createEdit.cshtml", model);
		}
	}
}