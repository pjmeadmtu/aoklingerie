﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
    public class SourceClassMapping : BaseEntity
    {
		public string Name { get; set; }
		public int SourceId { get; set; }
		public ImportEntityType EntityType { get; set; }

		public virtual ImportSource ImportSource { get; set; }
		public virtual ICollection<SourceMapping> SourceMappings { get; set; }
		public virtual ICollection<SourceKeyColumn> SourceKeyColumns { get; set; }
	}
}