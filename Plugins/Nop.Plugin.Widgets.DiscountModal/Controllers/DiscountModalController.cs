﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;

using Nop.Services.Configuration;
using Nop.Services.Discounts;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

using Nop.Plugin.Widgets.DiscountModal.Models;
using Nop.Services.Messages;
using Nop.Services.Stores;
using Nop.Core;
using Nop.Plugin.Widgets.DiscountModal.Domain.Services;
using System.Net;

namespace Nop.Plugin.Widgets.DiscountModal.Controllers
{
	public class DiscountModalController : BasePluginController
	{
		readonly ISettingService _settingService;
		readonly IPermissionService _permissionService;
		readonly IDiscountService _discountService;
		readonly ICampaignService _campaignService;
		readonly IStoreService _storeService;
		readonly IWorkContext _workContext;
		readonly IDiscountMessageService _discountMessageService;

		public DiscountModalController(
			ISettingService settingService,
			IPermissionService permissionService,
			IDiscountService discountService,
			ICampaignService campaignService,
			IStoreService storeService,
			IWorkContext workContext,
			IDiscountMessageService discountMessageService)
		{
			_settingService = settingService;
			_permissionService = permissionService;
			_discountService = discountService;
			_campaignService = campaignService;
			_storeService = storeService;
			_workContext = workContext;
			_discountMessageService = discountMessageService;
		}

		[AuthorizeAdmin]
		[Area(AreaNames.Admin)]
		public IActionResult Configure()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
				return AccessDeniedView();

			int storeId = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			DiscountModalSettings modalSettings = _settingService.LoadSetting<DiscountModalSettings>();
			IList<SelectListItem> selectListItems = _discountService.GetAllDiscounts().Where(p => !string.IsNullOrEmpty(p.CouponCode)).Select(discount =>
			{
				return new SelectListItem
				{
					Selected = discount.Id.Equals(modalSettings.DiscountId),
					Text = discount.Name,
					Value = discount.Id.ToString()
				};
			}).ToList();

			IList<SelectListItem> messageTemplates = _campaignService.GetAllCampaigns(storeId).OrderBy(p => p.Name)
				.Select(message =>
				{
					return new SelectListItem {
						Selected = message.Id.Equals(modalSettings.CampaignId),
						Text = message.Name,
						Value = message.Id.ToString()
					};
				}).ToList();

			//TO DO: Update these as resource fields if you decide to resell
			selectListItems.Insert(0, new SelectListItem { Text = "- Select Discount -", Value = "-1" });
			messageTemplates.Insert(0, new SelectListItem { Text = "- Select Message Template -", Value = "-1" });

			ConfigurationModel configModel = new ConfigurationModel
			{
				ButtonText = modalSettings.ButtonText,
				CancelMessage = modalSettings.CancelMessage,
				ClosingText = modalSettings.ClosingText,
				CustomCss = modalSettings.CustomCss,
				Message = modalSettings.Message,
				PlaceHolderText = modalSettings.PlaceHolderText,
				Title = modalSettings.Title,
				DiscountList = selectListItems,
				Campaigns = messageTemplates,
				DiscountId = modalSettings.DiscountId,
				CampaignId = modalSettings.CampaignId,
				Delay = modalSettings.Delay,
				ThanksForSubscribing = modalSettings.ThanksForSubscribing
			};

			return View($"{Consts.ViewsPath}Configure.cshtml", configModel);
		}

		[HttpPost]
		[AuthorizeAdmin]
		[Area(AreaNames.Admin)]
		public IActionResult Configure(ConfigurationModel configurationModel)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
				return AccessDeniedView();

			DiscountModalSettings modalSettings = _settingService.LoadSetting<DiscountModalSettings>();

			// Need to figure out localization if you're going to create a reusable modal
			modalSettings.ButtonText = configurationModel.ButtonText;
			modalSettings.CancelMessage = configurationModel.CancelMessage;
			modalSettings.ClosingText = configurationModel.ClosingText;
			modalSettings.CustomCss = configurationModel.CustomCss;
			modalSettings.Message = configurationModel.Message;
			modalSettings.PlaceHolderText = configurationModel.PlaceHolderText;
			modalSettings.Title = configurationModel.Title;
			modalSettings.DiscountId = configurationModel.DiscountId;
			modalSettings.CampaignId = configurationModel.CampaignId;
			modalSettings.LastUpdate = DateTime.UtcNow;
			modalSettings.Delay = configurationModel.Delay;
			modalSettings.ThanksForSubscribing = configurationModel.ThanksForSubscribing;

			_settingService.SaveSetting(modalSettings);
			_settingService.ClearCache();

			// TO DO: Update for localization if you're going to resell this
			SuccessNotification("Discount Modal Settings Saved");

			return Configure();
		}

		[Route("mutableideas/discountmodal/modal-css")]
		public IActionResult GetModalCss()
		{
			var modalSettings = _settingService.LoadSetting<DiscountModalSettings>();
			Response.Headers.Add("Cache-Control", new StringValues("public"));
			Response.ContentType = "text/css";

			return View($"{Consts.ViewsPath}CustomCss.cshtml", modalSettings.CustomCss);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Route("mutableideas/discountmodal/subscribe-discount")]
		public IActionResult Subscribe([FromBody] string email)
		{
			email = email.Trim();

			int storeId = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			bool hasSubscribed = _discountMessageService.HasEmailSubscribed(email, storeId);

			if (hasSubscribed)
				return StatusCode((int)HttpStatusCode.BadRequest, $"{email} has already subscribed.");

			_discountMessageService.SendDiscountMessage(email, storeId);
			return Ok();
		}
	}
}
