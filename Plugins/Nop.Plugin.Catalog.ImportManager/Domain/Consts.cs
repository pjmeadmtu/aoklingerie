﻿namespace Nop.Plugin.Catalog.ImportManager.Domain
{
	public static class Consts
	{
		public const string ViewsPath = "~/Plugins/Catalog.ImportManager/Views";
		public const string ScriptsPath = "/Plugins/Catalog.ImportManager/Scripts";
		public static string SharedPath = $"{ViewsPath}/shared";

		public const string MUTABLE_IDEAS_PREFIX = "MI_ImportMgr_";
	}
}
