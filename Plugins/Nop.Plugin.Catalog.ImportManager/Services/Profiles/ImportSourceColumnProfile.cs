﻿using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class ImportSourceColumnProfile : Profile
	{
		public ImportSourceColumnProfile()
		{
			CreateMap<ImportSourceColumn, ImportSourceColumnModel>();
			CreateMap<ImportSourceColumnModel, ImportSourceColumn>()
				.Ignore(p => p.ImportSource);
		}
	}
}