﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Messages;

namespace Nop.Plugin.Widgets.DiscountModal
{
	public class DiscountModalPlugin : BasePlugin, IWidgetPlugin
	{
		readonly IWebHelper _webHelper;

		public DiscountModalPlugin(IWebHelper webHelper)
		{
			_webHelper = webHelper;
		}

		public void GetPublicViewComponent(string widgetZone, out string viewComponentName)
		{
			viewComponentName = "DiscountModal";
		}

		public override string GetConfigurationPageUrl()
		{
			return _webHelper.GetStoreLocation() + "Admin/DiscountModal/Configure";
		}

		public IList<string> GetWidgetZones()
		{
			return new List<string> { "body_end_html_tag_before" };
		}
	}
}
