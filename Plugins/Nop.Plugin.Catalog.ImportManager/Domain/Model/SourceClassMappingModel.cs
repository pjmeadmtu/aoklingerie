﻿using System.Collections.Generic;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
    public class SourceClassMappingModel : BaseEntityModel
    {
		public SourceClassMappingModel()
		{
			SourceMappings = new List<int>();
		}

		public string Name { get; set; }
		public int SourceId { get; set; }
		public ImportEntityType EntityType { get; set; }
		public IEnumerable<int> SourceMappings { get; set; }
		public IEnumerable<int> SourceKeyMappings { get; set; }
	}
}