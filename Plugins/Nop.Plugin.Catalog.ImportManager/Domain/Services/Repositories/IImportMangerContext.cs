﻿using Nop.Data;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories
{
	public interface IImportManagerContext : IDbContext
	{
		void Install();
		void Uninstall();
	}
}