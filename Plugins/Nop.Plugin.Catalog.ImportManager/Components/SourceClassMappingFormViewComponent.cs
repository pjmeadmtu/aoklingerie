﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "SourceClassMappingForm")]
	public class SourceClassMappingFormViewComponent : NopViewComponent
	{
		public SourceClassMappingFormViewComponent() { }

		public IViewComponentResult Invoke()
		{
			return View($"{SharedPath}/SourceClassMappingForm.cshtml");
		}
	}
}