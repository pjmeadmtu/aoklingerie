﻿using Nop.Core.Domain.Media;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.Doba.Domain.Services.Import
{
    public interface IImageImportService
    {
		Task<IEnumerable<Picture>> ImportPictures(IEnumerable<string> urls);
    }
}
