﻿using System;

namespace Nop.Plugin.Catalog.Doba.Domain.Models
{
	public class DobaProductModel : BaseEntityModel
	{
		private decimal _cost => ShipCost + DropshipFee + _transactionFee;

		// Square's Transaction Fees
		private decimal _transactionFee => (MSRP * 0.03M) + .3M;

		public string ItemId { get; set; }
		public string Sku { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public decimal Price { get; set; }
		public decimal ShipCost { get; set; }
		public decimal DropshipFee { get; set; }
		public int QuantityAvailable { get; set; }
		public int ProductId { get; set; }
		public int? InternalProductId { get; set; }
		public decimal ShipWeight { get; set; }
		public decimal MSRP { get; set; }
		public DateTime? EstAvail { get; set; }
		public string ImageFile { get; set; }
		public string AdditionalImages { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime? LastUpdated { get; set; }
		public decimal PrepayPrice { get; set; }
		public string SupplierName { get; set; }

		// Values for Binding
		public bool IsNew => InternalProductId.HasValue;
		public bool ImportProduct { get; set; }

		public decimal PrepayCost => _cost + PrepayPrice;
		public decimal ListCost => _cost + Price;

		public decimal ProfitMarginPrice => MSRP - ListCost;
		public decimal ProfitMarginPrepay => MSRP - PrepayCost;
		public decimal ProfitMarginPercentagePrepay => ProfitMarginPrepay / PrepayCost;
		public decimal ProfitMarginPercentageList => ProfitMarginPrice / ListCost;
	}
}