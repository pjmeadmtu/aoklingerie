﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.DiscountModal.Domain.Services;
using Nop.Plugin.Widgets.DiscountModal.Models;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.DiscountModal.Components
{
	[ViewComponent(Name = "DiscountModal")]
	public class DiscountModalViewComponent : NopViewComponent
	{
		readonly IDiscountMessageService _discountModalService;

		public DiscountModalViewComponent(IDiscountMessageService discountMessageService)
		{
			_discountModalService = discountMessageService;
		}

		public virtual IViewComponentResult Invoke(string widgetZone, object additionalData)
		{
			DiscountModalSettingsModel model = _discountModalService.GetDiscountModalSettingsModel();
			return View($"{Consts.ViewsPath}PublicInfo.cshtml", model);
		}
	}
}