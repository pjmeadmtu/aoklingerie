﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[Route("admin/mutableideas/source-mapping")]
    public class SourceMappingController : BaseCrudController<SourceMapping, SourceMappingModel>
    {
		readonly ISourceMappingService _sourceMappingService;

		public SourceMappingController(ISourceMappingService service) : base(service)
		{
			_sourceMappingService = service;
		}

		[HttpGet("property-names/{entityType}")]
		public IActionResult GetPropertyNames(ImportEntityType entityType)
		{
			IEnumerable<string> propertyNames = _sourceMappingService.GetPropertyNames(entityType);
			return Ok(propertyNames);
		}
	}
}