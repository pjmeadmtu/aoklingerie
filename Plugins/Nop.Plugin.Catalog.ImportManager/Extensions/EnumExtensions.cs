﻿using System;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class EnumExtensions
	{
		public static string GetName<T>(this T enumItem)
			where T : struct
		{
			return Enum.GetName(typeof(T), enumItem);
		}
	}
}
