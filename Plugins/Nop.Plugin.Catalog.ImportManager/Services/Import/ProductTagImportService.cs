﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ProductTagImportService : BaseImportService<ProductTag>, IProductTagImportService
	{
		public ProductTagImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			IProductTagService productTagService)
			: base(excelExtractService,
				sourceMappingRefService,
				ImportEntityType.ProductTag,
				productTagService.InsertProductTag,
				productTagService.UpdateProductTag) { }
	}
}