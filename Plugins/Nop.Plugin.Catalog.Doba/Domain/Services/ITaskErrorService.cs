﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.Doba.Domain.Services
{
    public interface ITaskErrorService
    {
		void HandleError(Task task);
		void HandleErrors(IEnumerable<Task> tasks);
    }
}