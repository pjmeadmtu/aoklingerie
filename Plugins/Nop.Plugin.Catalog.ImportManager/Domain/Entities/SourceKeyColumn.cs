﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class SourceKeyColumn : BaseEntity
	{
		public int? SourceColumnId { get; set; }
		public int SourceClassMappingId { get; set; }
		public int? ReferenceClassMappingId { get; set; }
		public string Name { get; set; }
		public ColumnType ColumnType { get; set; }

		public virtual ImportSourceColumn SourceColumn { get; set; }
		public virtual SourceClassMapping SourceClassMapping { get; set; }
		public virtual SourceClassMapping ReferenceClassMapping { get; set; }
	}
}