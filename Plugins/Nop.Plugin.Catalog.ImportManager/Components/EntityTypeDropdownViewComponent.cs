﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "EntityTypeDropdown")]
	public class EntityTypeDropdownViewComponent : EnumDropdownViewComponent<ImportEntityType> { }
}