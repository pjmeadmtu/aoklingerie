﻿var importCollectionService = function (collectionSubscription) {
	var service = new baseService('import-collection', collectionSubscription);
	return $.extend(service, {
		import: function (collectionId) {
			return this.get('import/' + collectionId, 'IMPORT');
		}
	});
};