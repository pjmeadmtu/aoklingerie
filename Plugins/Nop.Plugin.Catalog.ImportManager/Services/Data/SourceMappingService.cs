﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;

using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class SourceMappingService : BaseDataService<SourceMapping, SourceMappingModel>, ISourceMappingService
	{
		readonly ISourceMappingRepository _sourceMappingRepository;
		readonly ISourceKeyColumnService _sourceKeyColumnService;
		readonly IMapper _mapper;

		public SourceMappingService(
			ISourceMappingRepository repository,
			IQueryStringExpressionService<SourceMapping> qsExpressionService,
			IMapper mapper,
			ISourceKeyColumnService sourceKeyColumnService)
			: base(repository, qsExpressionService, mapper)
		{
			_sourceKeyColumnService = sourceKeyColumnService;
			_sourceMappingRepository = repository;
			_mapper = mapper;
		}

		public IEnumerable<string> GetPropertyNames(ImportEntityType entityType)
		{
			Type itemType;
			switch (entityType)
			{
				case ImportEntityType.Inventory:
					itemType = typeof(ProductWarehouseInventory);
					break;
				case ImportEntityType.Product:
					itemType = typeof(Product);
					break;
				case ImportEntityType.ProductPicture:
					itemType = typeof(ProductPicture);
					break;
				case ImportEntityType.ProductAttribute:
					itemType = typeof(ProductAttribute);
					break;
				case ImportEntityType.ProductAttributeValue:
					itemType = typeof(ProductAttributeValue);
					break;
				case ImportEntityType.SpecificationAttribute:
					itemType = typeof(SpecificationAttribute);
					break;
				case ImportEntityType.SpecificationAttributeOption:
					itemType = typeof(SpecificationAttributeOption);
					break;
				case ImportEntityType.ProductAttributeMapping:
					itemType = typeof(ProductAttributeMapping);
					break;
				case ImportEntityType.ProductSpecificationMapping:
					itemType = typeof(ProductSpecificationAttribute);
					break;
				case ImportEntityType.ProductTag:
					itemType = typeof(ProductTag);
					break;
				case ImportEntityType.Category:
					itemType = typeof(Category);
					break;
				case ImportEntityType.CategoryMap:
					itemType = typeof(ProductCategory);
					break;
				case ImportEntityType.ProductAttributeCombination:
					itemType = typeof(ProductAttributeCombination);
					break;
				default:
					throw new ArgumentException("The Import Entity Type supplied is not valid.");
			}

			IEnumerable<string> propertyNames = ObjectExtensions.GetPublicReadWrite(itemType)
				.Where(p => p.PropertyType == typeof(string) || p.PropertyType.IsValueType)
				.Select(p => p.Name)
				.OrderBy(p => p).ToArray();

			return propertyNames;
		}
	}
}