﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class SpecificationAttributeImportService : BaseImportService<SpecificationAttribute>, ISpecificationAttributeImportService
	{
		public SpecificationAttributeImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			ISpecificationAttributeService productService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.SpecificationAttribute,
				  productService.InsertSpecificationAttribute,
				  productService.UpdateSpecificationAttribute) { }
	}
}