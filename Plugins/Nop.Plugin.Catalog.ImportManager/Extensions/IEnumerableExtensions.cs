﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class IEnumerableExtensions
	{
		public static void Iterate<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			foreach (T item in enumerable)
				action(item);
		}

		// Pages enumerables, pages start at page 1
		public static IEnumerable<T> Page<T>(this IEnumerable<T> enumerable, int pageSize, int page)
		{
			if (page < 1 || pageSize < 1)
				throw new ArgumentException("Page arguments must be greater than 0.");

			return enumerable.Skip((page - 1) * pageSize).Take(pageSize);
		}

		public static bool NotAny<T>(this IEnumerable<T> enumerable)
		{
			return !enumerable.Any();
		}

		public static bool NotAny<T>(this IEnumerable<T> enumerable, Func<T, bool> filter)
		{
			return !enumerable.Any(filter);
		}
	}
}