﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class ImportSourceColumnMap : NopEntityTypeConfiguration<ImportSourceColumn>
	{
		public ImportSourceColumnMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}ImportSourceColumn");
			HasKey(p => p.Id);

			HasRequired(p => p.ImportSource)
				.WithMany(p => p.ImportSourceColumns)
				.HasForeignKey(p => p.SourceId)
				.WillCascadeOnDelete(true);

			Property(p => p.Index).IsRequired();
			Property(p => p.Name).HasMaxLength(100).IsRequired();
		}
	}
}