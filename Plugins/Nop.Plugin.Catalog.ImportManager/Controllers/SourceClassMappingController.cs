﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[Route("admin/mutableideas/source-class-mapping")]
	public class SourceClassMappingController : BaseCrudController<SourceClassMapping, SourceClassMappingModel>
	{
		readonly ISourceClassMappingService _sourceClassMappingService;

		public SourceClassMappingController(ISourceClassMappingService service)
			: base(service)
		{
			_sourceClassMappingService = service;
		}

		[HttpGet("by-collection/{collectionId}")]
		public IActionResult GetByForCollection(int collectionId)
		{
			IEnumerable<SourceClassMappingModel> models = _sourceClassMappingService.GetAllForCollection(collectionId)
				.ToArray();

			return Ok(models);
		}
	}
}
