﻿var emailRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

var queryStringExpressionService = function () {
	var _statements = [];

	var _transformValue = function (value) {
		// this is an array of values
		if (startsWith(value, '[') && endsWith(value, ']')) {
			return value;
		}

		return isNaN(Number(value)) ? "'" + value + "'" : parseInt(value);
	}

	var _buildStatement = function (propertyName, filterType, value, comparison) {
		var transformedValue = _transformValue(value);
		var values = [propertyName, filterType, encodeURI(transformedValue)];
		if (!isEmpty(comparison))
			values.unshift(comparison);

		_statements.push(values.join(' '));
	}

	var createQstringArray = function (values) {
		var qStringValues = values.map(function (value) {
			return _transformValue(value);
		});

		return '[' + qStringValues.join(',') + ']';
	}

	var by = function (propertyName, filterType, value) {
		_buildStatement(propertyName, filterType, value);
		return this;
	}

	var and = function (propertyName, filterType, value) {
		_buildStatement(propertyName, filterType, value, ' and');
		return this;
	}

	var or = function (propertyName, filterType, value) {
		_buildStatement(propertyName, filterType, value, ' or');
		return this;
	}

	var build = function () {
		var qstring = _statements.join('');
		_statements = [];
		return qstring;
	}

	return {
		by: by,
		or: or,
		and: and,
		createQstringArray: createQstringArray,
		build: build
	};
};

function isEmpty(obj) {
	return obj === undefined
		|| obj === null
		|| (($.isArray(obj) || typeof obj === 'string') && obj.length === 0);
}

function isEmptyOrWhiteSpace(obj) {
	return isEmpty(obj) || (typeof obj === 'string' && obj.replace(/\s+/g, '').length === 0);
}

function startsWith(stringValue, value) {
	return isString(stringValue) && stringValue.indexOf(value) === 0;
}

function endsWith(stringValue, value) {
	return isString(stringValue) && stringValue.indexOf(value) === stringValue.length - 1;
}

function isString(value) {
	return typeof (value) === "string";
}

function isNumber(value) {
	return !isEmpty(value) && !isNaN(value);
}

function isEmail(value) {
	return emailRegEx.test(value);
}

function trim(value) {
	return $.trim(value);
}
