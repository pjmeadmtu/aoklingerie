﻿	CREATE TABLE [dbo].[DobaProduct](
		[Id] [int] NOT NULL,
		[ImageFile] [varchar](400) NOT NULL,
		[AdditionalImages] [nvarchar](4000) NULL,
		[DateCreated] [datetime] NOT NULL,
		[DropShipFee] [decimal](18, 0) NOT NULL,
		[EstAvail] [datetime] NULL,
		[ItemId] [varchar](50) NOT NULL,
		[LastUpdated] [datetime] NULL,
		[Price] [decimal](18, 0) NOT NULL,
		[PrepayPrice] [decimal](18, 0) NOT NULL,
		[ProductId] [int] NOT NULL,
		[InternalProductId] [int] NULL,
		[QuantityAvailable] [int] NOT NULL,
		[ShipCost] [decimal](18, 0) NOT NULL,
		[ShipWeight] [decimal](18, 0) NULL,
		[Sku] [nvarchar](400) NULL,
		[Title] [nvarchar](400) NULL,
	 CONSTRAINT [PK_DobaProduct] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
