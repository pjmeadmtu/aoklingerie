﻿using System.Collections.Generic;

using Nop.Core.Plugins;
using Nop.Services.Cms;

namespace Nop.Plugin.Widgets.MutableIdeas.Core
{
	public class MutableIdeasCorePlugin : BasePlugin, IWidgetPlugin
	{
		public void GetPublicViewComponent(string widgetZone, out string viewComponentName)
		{
			viewComponentName = Consts.FRONTENDCORE;
		}

		public IList<string> GetWidgetZones()
		{
			return new List<string>() { "body_start_html_tag_after" };
		}
	}
}