﻿using System;
using System.Collections.Generic;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface IKeyGeneratorService
	{
		string GenerateKey(IEnumerable<SourceKeyColumn> sourceKeyColumnModels,
			Func<ImportSourceColumn, ColumnType, object> getValue);
	}
}