﻿using System;
using System.Linq;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Extensions
{
	public static class IQueryCollectionExtension
	{
		public static bool TryGetValue<T>(this IQueryCollection collection, string key, out T value)
		{
			StringValues values;
			value = default(T);

			if (collection.TryGetValue(key, out values) && values.Count() > 0)
			{
				value = (T)Convert.ChangeType(values.First(), typeof(T));
				return true;
			}

			return false;
		}

		public static PagingOptions GetPagingOptions(this IQueryCollection collection)
		{
			collection.TryGetValue("filter", out string filter);
			collection.TryGetValue("orderBy", out string orderBy);

			collection.TryGetValue("page", out int page);
			collection.TryGetValue("pageSize", out int pageSize);

			return new PagingOptions
			{
				Filter = filter,
				Page = page,
				PageSize = pageSize,
				OrderBy = orderBy
			};
		}
	}
}
