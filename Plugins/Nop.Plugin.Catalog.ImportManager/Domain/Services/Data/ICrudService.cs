﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface ICrudService<T, TModel>
		where T : BaseEntity
		where TModel : BaseEntityModel
	{
		IEnumerable<TModel> Load();
		IEnumerable<TModel> Load(IEnumerable<int> ids);
		TModel GetById(int id);
		IEnumerable<TModel> Save(IEnumerable<TModel> models);
		IEnumerable<TModel> Filter(string filter);
		IEnumerable<TModel> Filter(Expression<Func<T, bool>> expression);
		PageModel<TModel> GetPagedModel(PagingOptions pageOptions);

		TModel Insert(TModel model);
		TModel Update(TModel model);

		void Delete(int id);
		void Delete(IEnumerable<int> ids);
	}
}