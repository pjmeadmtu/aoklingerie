﻿var baseService = function (entityName, collectionSubscription) {
	var baseUrl = '/admin/mutableideas/' + entityName;
	var restService = new executeRequest(baseUrl);
	var eventEntityName = entityName.replace(new RegExp('-', 'g'), '').toUpperCase();

	var _pushError = function (err) {
		collectionSubscription.push(_getEventName('ERROR', err));
	};

	var _getEventName = function (name) {
		return [eventEntityName, name].join('_');
	};

	var _executeDeferred = function (promise, evtName) {
		var deferred = $.Deferred();

		promise.then(function (result) {
			collectionSubscription.push(_getEventName(evtName), result);
			deferred.resolve(result);
		}, function (err) {
			_pushError(err);
			deferred.reject(err);
		}).done(function () {
			collectionSubscription.push(_getEventName(evtName + '_DONE'));
		});

		return deferred;
	};

	var getById = function (id) {
		return get(id, "GETBYID");
	};

	var get = function (url, evtName) {
		return _executeDeferred(restService.get(url), evtName);
	};

	var save = function (data, url) {
		return _executeDeferred(restService.save(data, url), "SAVED");
	};

	var remove = function (id, data) {
		return _executeDeferred(restService.remove(id, data), "REMOVE");
	};

	var removeMany = function (ids) {
		return _executeDeferred(restService.removeMany(ids), 'REMOVEMANY');
	};

	return {
		baseUrl: baseUrl,
		getById: getById,
		pushError: _pushError,
		get: get,
		save: save,
		remove: remove,
		removeMany: removeMany,
		_getEventName: _getEventName
	};
};