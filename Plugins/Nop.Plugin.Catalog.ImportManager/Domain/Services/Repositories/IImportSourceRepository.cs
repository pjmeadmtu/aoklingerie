﻿using Nop.Core.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories
{
	public interface IImportSourceRepository : IRepository<ImportSource> { }
}