﻿namespace Nop.Plugin.Widgets.MutableIdeas.Core
{
	public static class Consts
	{
		public const string FRONTENDCORE = "FRONTENDCOREVIEWCOMPONENT";
		public const string SCRIPTS_PATH = "Plugins/Widgets.MutableIdeas.Core/Content/scripts";
		public const string COMPONENTS_PATH = "~/Plugins/Widgets.MutableIdeas.Core/Views";
	}
}