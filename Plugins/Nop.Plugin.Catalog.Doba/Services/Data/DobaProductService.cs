﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.Doba.Domain.Entities;
using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Domain.Repositories;
using Nop.Plugin.Catalog.Doba.Domain.Services;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;
using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Plugin.Catalog.Doba.Extensions;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.Doba.Services.Data
{
	public class DobaProductService : BaseDataService<DobaProduct, DobaProductModel>, IDobaProductService
	{
		readonly IMapper _mapper;
		readonly IProductService _productService;
		readonly IDobaProductRepository _dobaProductRepository;
		readonly IProductImageImportService _productImageImportService;
		readonly ITaskErrorService _taskErrorService;

		public DobaProductService(
			IDobaProductRepository repository,
			IMapper mapper,
			IProductService productService,
			IProductImageImportService productImageImportService,
			ITaskErrorService taskErrorService)
			: base(repository, mapper)
		{
			_mapper = mapper;
			_dobaProductRepository = repository;
			_productImageImportService = productImageImportService;
			_productService = productService;
			_taskErrorService = taskErrorService;
		}

		public IEnumerable<DobaProductModel> ConvertImportModels(IEnumerable<DobaProductImportModel> importModels)
		{
			IDictionary<string, DobaProductImportModel> dobaItems = importModels.ToDictionary(p =>
				p.ItemId, p => p);
			IDictionary<string, DobaProductModel> existingModels = Where(p => dobaItems.Keys.Contains(p.ItemId))
				.ToDictionary(p => p.ItemId, p => p);

			return importModels.Select(import => _mapper.Map<DobaProductModel>(import)).Select(productModel =>
			{
				DobaProductModel existingModel;
				if (existingModels.TryGetValue(productModel.ItemId, out existingModel))
				{
					existingModel.QuantityAvailable = productModel.QuantityAvailable;
					existingModel.ShipCost = productModel.ShipCost;
					existingModel.MSRP = productModel.MSRP;
					existingModel.EstAvail = productModel.EstAvail;
					existingModel.Price = productModel.Price;
					existingModel.LastUpdated = DateTime.UtcNow;

					return existingModel;
				}

				productModel.LastUpdated = DateTime.UtcNow;
				productModel.DateCreated = DateTime.UtcNow;
				return productModel;
			}).ToArray();
		}

		public async Task Import(IEnumerable<int> ids)
		{
			IEnumerable<DobaProduct> dobaProducts = _dobaProductRepository.Where(p => ids.Contains(p.Id)).ToArray();
			IEnumerable<Product> products = dobaProducts.Select(dobaProduct => {
				Product product = _mapper.Map<Product>(dobaProduct);
				_productService.InsertProduct(product);

				dobaProduct.InternalProductId = product.Id;
				_dobaProductRepository.Update(dobaProduct);

				return product;
			}).ToArray();

			Task[] tasks = dobaProducts.Select(async dobaProduct =>
			{
				await _productImageImportService.ImportImages(dobaProduct.InternalProductId.Value, new[] { dobaProduct.ImageFile });
			}).ToArray();

			await Task.Factory.ContinueWhenAll(tasks, completedTasks =>
			{
				_taskErrorService.HandleErrors(completedTasks);
			});
		}

		public override void Delete(IEnumerable<int> ids)
		{
			IEnumerable<int> productIds = Load(ids)
				.Where(p => p.InternalProductId.HasValue)
				.Select(p => p.InternalProductId.Value).ToArray();

			IEnumerable<Product> products = _productService.Where(p => productIds.Contains(p.Id)).ToArray();
			products.Iterate(_productService.DeleteProduct);

			base.Delete(ids);
		}
	}
}