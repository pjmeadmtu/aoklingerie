﻿using System.Collections.Generic;
using Nop.Core;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Import
{
	public interface IExtractService
	{
		IDictionary<string, T> Extract<T>(int sourceMappingClassId) where T : BaseEntity, new();
	}
}