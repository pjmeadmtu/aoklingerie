﻿using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class SpecificationAttributeOptionImportService : BaseImportService<SpecificationAttributeOption>, ISpecificationAttributeOptionImportService
	{
		public SpecificationAttributeOptionImportService(
			IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			ISpecificationAttributeService specificationAttributeService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.SpecificationAttributeOption,
				  specificationAttributeService.InsertSpecificationAttributeOption,
				  specificationAttributeService.UpdateSpecificationAttributeOption) { }
	}
}