﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using AutoMapper;
using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;

namespace Nop.Plugin.Catalog.Doba.Services.Data
{
	public abstract class BaseDataService<T, TModel> : ICrudService<T, TModel>
		where T : BaseEntity
		where TModel : BaseEntityModel
	{
		readonly IRepository<T> _repository;
		readonly IMapper _mapper;

		protected BaseDataService(IRepository<T> repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		public virtual void Delete(int id)
		{
			T entity = _repository.Where(p => p.Id == id).FirstOrDefault();
			_repository.Delete(entity);
		}

		public virtual void Delete(IEnumerable<int> ids)
		{
			IEnumerable<T> entities = _repository.Where(p => ids.Contains(p.Id)).ToArray();
			_repository.Delete(entities);
		}

		public virtual TModel GetById(int id)
		{
			T entity = _repository.GetById(id);
			return _mapper.Map<TModel>(entity);
		}

		public virtual TModel Insert(TModel model)
		{
			T entity = _mapper.Map<T>(model);
			_repository.Insert(entity);

			return _mapper.Map<TModel>(entity);
		}

		public IEnumerable<TModel> Load()
		{
			return Map(() => _repository.Table);
		}

		public virtual IEnumerable<TModel> Load(IEnumerable<int> ids)
		{
			return Where(p => ids.Contains(p.Id));
		}

		public virtual IEnumerable<TModel> Save(IEnumerable<TModel> models)
		{
			IEnumerable<T> updatedEntities = Map(() => models.Where(p => p.Id.HasValue).ToArray()).ToArray();
			IEnumerable<T> insertEntities = Map(() => models.Where(p => !p.Id.HasValue).ToArray()).ToArray();

			_repository.Update(updatedEntities);
			_repository.Insert(insertEntities);

			insertEntities = insertEntities.Concat(updatedEntities);
			return Map(() => insertEntities);
		}

		public virtual TModel Update(TModel model)
		{
			T entity = _mapper.Map<T>(model);
			_repository.Update(entity);

			return model;
		}

		public virtual IEnumerable<TModel> Where(Expression<Func<T, bool>> expression)
		{
			return Map(() => _repository.Where(expression));
		}

		protected IEnumerable<TModel> Map(Func<IEnumerable<T>> func)
		{
			IEnumerable<T> entities = func();
			return entities.Select(p => _mapper.Map<TModel>(p));
		}

		protected IEnumerable<T> Map(Func<IEnumerable<TModel>> func)
		{
			IEnumerable<TModel> models = func();
			return models.Select(p => _mapper.Map<T>(p));
		}
	}
}
