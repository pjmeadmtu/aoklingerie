﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Extensions;

using static Nop.Plugin.Catalog.ImportManager.Domain.Enums.ColumnType;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class KeyGeneratorService : IKeyGeneratorService
	{
		readonly ISourceMappingRefService _sourceMappingRefService;
		readonly IDictionary<ImportEntityType, IDictionary<string, int>> _entitiesDictionaryReferences;

		public KeyGeneratorService(ISourceMappingRefService sourceMappingRefService)
		{
			_sourceMappingRefService = sourceMappingRefService;
			_entitiesDictionaryReferences = new Dictionary<ImportEntityType, IDictionary<string, int>>();
		}

		public string GenerateKey(IEnumerable<SourceKeyColumn> sourceKeyColumns, Func<ImportSourceColumn, ColumnType, object> getValue)
		{
			var sb = new StringBuilder();

			sourceKeyColumns = sourceKeyColumns.OrderBy(p => p.Id);
			foreach (SourceKeyColumn keyColumn in sourceKeyColumns)
				Append(sb, GetKeyValue(keyColumn, getValue));

			return sb.ToString();
		}

		string GetKeyValue(SourceKeyColumn model, Func<ImportSourceColumn, ColumnType, object> getValue)
		{
			string value = string.Empty;

			switch (model.ColumnType)
			{
				case ColumnNameAsValue:
				case ColumnNameAsIdentifier:
					value = getValue(model.SourceColumn, ColumnNameAsIdentifier).ToString();
					break;
				case CompositeKeyReferenceValue:
					string referenceKey = GenerateKey(model.ReferenceClassMapping.SourceKeyColumns, getValue);
					value = GetRowValueIdentifierKey(model.ReferenceClassMapping.SourceId, referenceKey, model.ReferenceClassMapping.EntityType);
					break;
				case CompositeKeyValue:
				case RowValueIdentifier:
					value = getValue(model.SourceColumn, RowValue).ToString();
					break;
			}

			return value.SourceKey();
		}

		string GetRowValueIdentifierKey(int sourceId, string value, ImportEntityType entityType)
		{
			IDictionary<string, int> dict = GetEntitiesDictionary(sourceId, entityType);
			if (dict.TryGetValue(value.SourceKey(), out int key)) 
				return string.Format("{0}_{1}", entityType.GetName().SourceKey(), key).ToString();

			return string.Empty;
		}

		IDictionary<string, int> GetEntitiesDictionary(int sourceId, ImportEntityType entityType)
		{
			if (_entitiesDictionaryReferences.ContainsKey(entityType))
				return _entitiesDictionaryReferences[entityType];

			IDictionary<string, int> dict = _sourceMappingRefService.Filter(p => p.SourceId == sourceId
				&& p.EntityType == entityType).ToDictionary(p => p.SourceIdValue, p => p.EntityId);

			_entitiesDictionaryReferences.Add(entityType, dict);
			return dict;
		}

		static void Append(StringBuilder sb, string value)
		{
			if (sb.Length > 0)
				sb.Append("_");

			sb.Append(value);
		}
	}
}
