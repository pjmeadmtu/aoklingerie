﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class ImportService : IImportService
	{
		readonly IImportCollectionService _importCollectionService;
		readonly IImportSourceService _importSourceService;
		readonly ISourceClassMappingService _sourceClassMappingService;
		readonly IImportEntityServiceFactory _importServiceFactory;

		public ImportService(IImportCollectionService importCollectionService,
			IImportSourceService importSourceService,
			ISourceClassMappingService sourceClassMappingService,
			IImportEntityServiceFactory importServiceFactory)
		{
			_importCollectionService = importCollectionService;
			_importSourceService = importSourceService;
			_sourceClassMappingService = sourceClassMappingService;
			_importServiceFactory = importServiceFactory;
		}

		public void ImportCollection(int collectionId)
		{
			ImportCollectionModel collectionModel = _importCollectionService.GetById(collectionId);
			IEnumerable<ImportSourceModel> importSources = _importSourceService.Filter(p => p.ImportCollectionId == collectionId).ToArray();
			importSources.Iterate(source => ImportSource(source.Id.Value, source.SourceType));
		}

		void ImportSource(int importSourceId, SourceType sourceType)
		{
			IEnumerable<SourceClassMappingModel> sourceClassMappings = _sourceClassMappingService.Filter(p => p.ImportSource.Id == importSourceId).ToArray();

			sourceClassMappings.Iterate(sourceClassMapping =>
			{
				switch (sourceType)
				{
					case SourceType.CSV:
					case SourceType.Excel:
						ExtractForImportEntityType(sourceClassMapping);
						break;
					default:
						throw new NotImplementedException();
				}
			});
		}

		protected void ExtractForImportEntityType(SourceClassMappingModel sourceClassMapping)
		{
			IEntityImportService importEntityService = _importServiceFactory.GetImportService(sourceClassMapping.EntityType);
			importEntityService.Import(sourceClassMapping);
		}
	}
}