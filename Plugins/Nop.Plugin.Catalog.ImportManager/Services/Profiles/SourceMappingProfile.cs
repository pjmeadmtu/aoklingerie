﻿using AutoMapper;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Services.Profiles
{
	public class SourceMappingProfile : Profile
	{
		public SourceMappingProfile()
		{
			CreateMap<SourceMapping, SourceMappingModel>();
			CreateMap<SourceMappingModel, SourceMapping>()
				.Map(p => p.Id.HasValue ? p.Id.Value : -1, p => p.Id)
				.Ignore(p => p.SourceClassMapping);
		}
	}
}