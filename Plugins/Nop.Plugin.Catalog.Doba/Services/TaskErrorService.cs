﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Nop.Plugin.Catalog.Doba.Domain.Services;
using Nop.Services.Logging;

namespace Nop.Plugin.Catalog.Doba.Services
{
	public class TaskErrorService : ITaskErrorService
	{
		readonly ILogger _logger;
		object _loggerLock = new object();

		public TaskErrorService(ILogger logger)
		{
			_logger = logger;
		}

		public void HandleError(Task task)
		{
			if (!task.IsFaulted) return;

			lock (_loggerLock)
			{
				task.Exception.Handle(err =>
				{
					_logger.Error(err.Message, err);
					return true;
				});
			}
		}

		public void HandleErrors(IEnumerable<Task> tasks)
		{
			IEnumerable<Task> faultedTasks = tasks.Where(p => p.IsFaulted).ToArray();
			if (!faultedTasks.Any()) return;

			foreach (Task tsk in faultedTasks)
				HandleError(tsk);
		}
	}
}
