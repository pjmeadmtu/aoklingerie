﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Components
{
	[ViewComponent(Name = "ColumnTypeDropdown")]
	public class ColumnTypeDropdownViewComponent : EnumDropdownViewComponent<ColumnType> { }
}