﻿using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Widgets.DiscountModal.Domain.Services;
using Nop.Plugin.Widgets.DiscountModal.Services;

namespace Nop.Plugin.Widgets.DiscountModal.Infrastructure.IOC
{
	public class DependencyRegistrar : IDependencyRegistrar
	{
		public int Order => 1;

		public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
		{
			builder.RegisterType<DiscountMessageService>().As<IDiscountMessageService>().InstancePerLifetimeScope();
		}
	}
}