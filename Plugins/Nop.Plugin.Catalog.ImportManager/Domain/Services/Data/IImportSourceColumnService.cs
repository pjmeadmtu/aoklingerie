﻿using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Data
{
	public interface IImportSourceColumnService : ICrudService<ImportSourceColumn, ImportSourceColumnModel> { }
}
