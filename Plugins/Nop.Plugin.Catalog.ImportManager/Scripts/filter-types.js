﻿var filterType = {
	EQUAL: 'eq',
	LESSTHAN: 'lt',
	LESSTHANOREQUAL: 'lte',
	NOTEQUAL: 'ne',
	GREATERTHAN: 'gt',
	GREATHANOREQUAL: 'gte',
	CONTAINS: 'ct',
	CONTAINSIGNORECASE: 'ctic',
	IN: 'in',
	LENEQ: 'leneq',
	LENNE: 'lenne',
	LENGT: 'lengt',
	LENGTE: 'lengte',
	LENLT: 'lenlt',
	LENLTE: 'lenlte'
}