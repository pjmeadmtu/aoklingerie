﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Nop.Plugin.Catalog.Doba.Domain.Services.Import;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Plugin.Catalog.Doba.Domain.Models;
using Nop.Plugin.Catalog.Doba.Domain.Services.Data;

namespace Nop.Plugin.Catalog.Doba.Controllers
{
	[AuthorizeAdmin]
	[Route("admin/doba/import")]
	public class DobaImportController : BasePluginController
	{
		readonly IDobaImportService _dobaImportService;
		readonly IDobaProductService _dobaProductService;
		const string _viewsPath = "~/Plugins/Catalog.Doba/Views";

		public DobaImportController(IDobaImportService dobaImportService, IDobaProductService dobaProductService)
		{
			_dobaImportService = dobaImportService;
			_dobaProductService = dobaProductService;
		}

		[HttpGet]
		[Area(AreaNames.Admin)]
		public IActionResult Import()
		{
			return View($"{_viewsPath}/index.cshtml");
		}

		[HttpPost]
		[Area(AreaNames.Admin)]
		public async Task<IActionResult> Import(IFormFile xmlFileUpload)
		{
			await _dobaImportService.ImportList(xmlFileUpload.OpenReadStream());
			return View($"{_viewsPath}/index.cshtml");
		}

		[HttpGet("list")]
		[Area(AreaNames.Admin)]
		public IActionResult List()
		{
			IEnumerable<DobaProductModel> dobaProducts = _dobaProductService.Load();
			return Ok(dobaProducts);
		}

		[HttpPost("products")]
		[Area(AreaNames.Admin)]
		public async Task<IActionResult> ImportProducts(IEnumerable<int> selectedIds)
		{
			await _dobaProductService.Import(selectedIds);
			return Ok();
		}

		[HttpDelete("many")]
		[Area(AreaNames.Admin)]
		public IActionResult DeleteMany(IEnumerable<int> selectedIds)
		{
			_dobaProductService.Delete(selectedIds);
			return Ok();
		}
    }
}