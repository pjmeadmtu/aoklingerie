﻿namespace Nop.Plugin.Catalog.ImportManager.Domain.Model
{
	public class BaseEntityModel
	{
		public int? Id { get; set; }
	}
}