﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Extensions;

namespace Nop.Plugin.Catalog.ImportManager.Tests.Services
{
	[TestClass]
	public class KeyGeneratorServiceTest
	{
		IKeyGeneratorService _keyGenerator;
		
		[TestInitialize]
		public void Initialize()
		{
			var sourceMappingServiceMoq = new Mock<ISourceMappingRefService>();

			sourceMappingServiceMoq.Setup(p => p.Filter(It.IsAny<Expression<Func<SourceMappingRef, bool>>>())).Returns<Expression<Func<SourceMappingRef, bool>>>(expression => {
				return new List<SourceMappingRefModel>() {
					new SourceMappingRefModel { Id = 1, EntityId = 1, EntityType = ImportEntityType.Product, SourceId = 1, SourceIdValue = "GROUP1".SourceKey() },
					new SourceMappingRefModel { Id = 2, EntityId = 2, EntityType = ImportEntityType.Product, SourceId = 1, SourceIdValue = "GROUP2".SourceKey() },
					new SourceMappingRefModel { Id = 3, EntityId = 5, EntityType = ImportEntityType.Product, SourceId = 1, SourceIdValue = "GROUP3".SourceKey() },
					new SourceMappingRefModel { Id = 4, EntityId = 3, EntityType = ImportEntityType.Product, SourceId = 1, SourceIdValue = "GROUP4".SourceKey() }
				};
			});

			_keyGenerator = new KeyGeneratorService(sourceMappingServiceMoq.Object);
		}

		object GetValue(ImportSourceColumn sourceColumn, ColumnType columnType)
		{
			switch (columnType)
			{
				case ColumnType.ReferenceValue:
				case ColumnType.RowValue:
					return "GROUP1";
				case ColumnType.ColumnNameAsIdentifier:
					return sourceColumn.Name;
			}

			throw new InvalidOperationException();
		}

		/*[TestMethod]
		public void TestRowValueIdentifier()
		{
			var sourceKeyColumn = new SourceKeyColumn
			{
				Id = 1,
				ColumnType = ColumnType.RowValueIdentifier,
				SourceColumn = new ImportSourceColumn
				{
					Id = 1,
					Index = 2,
					Name = "GROUPING"
				}
			};

			string key = _keyGenerator.GenerateKey(new[] { sourceKeyColumn }, GetValue);
			key.Should().Be("group1");

			sourceKeyColumn.ColumnType = ColumnType.CompositeKeyValue;
			key = _keyGenerator.GenerateKey(new[] { sourceKeyColumn }, GetValue);
			key.Should().Be("group1");
		}

		[TestMethod]
		public void TestColumnNameAsId()
		{
			var columnKey = new SourceKeyColumn
			{
				Id = 1,
				ColumnType = ColumnType.ColumnNameAsIdentifier,
				SourceColumn = new ImportSourceColumn { Id = 1, Index = 2, Name = "Size", SourceId = 1 }
			};

			string key = _keyGenerator.GenerateKey(new[] { columnKey }, GetValue);
			key.Should().Be("size");
		}

		[TestMethod]
		public void TestCompositeKeyReferenceValue()
		{
			var columnKey = new SourceKeyColumn
			{
				Id = 1,
				ColumnType = ColumnType.CompositeKeyReferenceValue,
				EntityType = ImportEntityType.Product,
				SourceColumn = new ImportSourceColumn { Id = 1, Index = 2, Name = "Size", SourceId = 1 }
			};

			string key = _keyGenerator.GenerateKey(new[] { columnKey }, GetValue);
			key.Should().Be("product_1");
		}

		[TestMethod]
		public void TestCompositeKeyVale()
		{
			/*var columnKeys = new[] {
				new SourceKeyColumn
				{
					Id = 1,
					ColumnType = ColumnType.CompositeKeyReferenceValue,
					EntityType = ImportEntityType.Product,
					SourceColumn = new ImportSourceColumn { Id = 1, Index = 2, Name = "Size", SourceId = 1 }
				},
				new SourceKeyColumn
				{
					Id = 2,
					ColumnType = ColumnType.CompositeKeyValue,
					EntityType = ImportEntityType.Product,
					SourceColumn = new ImportSourceColumn { Id = 1, Index = 2, Name = "Size", SourceId = 1 }
				}
			};

			string key = _keyGenerator.GenerateKey(columnKeys, GetValue);
			key.Should().Be("product_1_group1");
		}*/
	}
}
