﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Catalog.ImportManager.Controllers
{
	[AuthorizeAdmin]
	[Area(AreaNames.Admin)]
	[Route("admin/mutableideas/import-source-column")]
	public class ImportSourceColumnController : BaseCrudController<ImportSourceColumn, ImportSourceColumnModel>
	{
		public ImportSourceColumnController(IImportSourceColumnService crudService) : base(crudService) { }

		public override IActionResult Delete(int id)
		{
			throw new InvalidOperationException();
		}

		public override IActionResult DeleteMany([FromBody] IEnumerable<int> selectedIds)
		{
			throw new InvalidOperationException();
		}

		public override IActionResult Post([FromBody] ImportSourceColumnModel model)
		{
			throw new InvalidOperationException();
		}

		public override IActionResult Put([FromBody] ImportSourceColumnModel model)
		{
			throw new InvalidOperationException();
		}

		public override IActionResult Save([FromBody] ImportSourceColumnModel model)
		{
			throw new InvalidOperationException();
		}
	}
}
