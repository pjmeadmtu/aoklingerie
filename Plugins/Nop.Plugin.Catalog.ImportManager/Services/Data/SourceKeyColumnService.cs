﻿using AutoMapper;
using MutableIdeas.Web.Linq.Query.Domain.Services;

using Nop.Plugin.Catalog.ImportManager.Domain.Entities;
using Nop.Plugin.Catalog.ImportManager.Domain.Model;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Repositories;

namespace Nop.Plugin.Catalog.ImportManager.Services.Data
{
	public class SourceKeyColumnService : BaseDataService<SourceKeyColumn, SourceKeyColumnModel>, ISourceKeyColumnService
	{
		readonly ISourceKeyColumnRepository _repository;
		readonly ISourceClassMappingRepository _sourceClassMappingRepository;
		readonly ISourceMappingRefService _sourceMappingRefService;

		public SourceKeyColumnService(ISourceKeyColumnRepository repository,
			ISourceClassMappingRepository sourceClassMappingRepository,
			IQueryStringExpressionService<SourceKeyColumn> querystringExpressionService,
			ISourceMappingRefService sourceMappingRefService,
			IMapper mapper) : base(repository, querystringExpressionService, mapper)
		{
			_repository = repository;
			_sourceMappingRefService = sourceMappingRefService;
			_sourceClassMappingRepository = sourceClassMappingRepository;
		}
	}
}