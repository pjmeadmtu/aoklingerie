﻿using Nop.Data.Mapping;
using Nop.Plugin.Catalog.ImportManager.Domain.Entities;

using static Nop.Plugin.Catalog.ImportManager.Domain.Consts;

namespace Nop.Plugin.Catalog.ImportManager.Infrastructure.Database.Mappings
{
	public class SourceMappingRefMap : NopEntityTypeConfiguration<SourceMappingRef>
	{
		public SourceMappingRefMap()
		{
			ToTable($"{MUTABLE_IDEAS_PREFIX}SourceMappingRef");
			HasKey(p => p.Id);

			HasRequired(p => p.ImportSource)
				.WithMany(p => p.SourceMappingRefs)
				.HasForeignKey(p => p.SourceId);

			Property(p => p.SourceIdValue).HasMaxLength(200).IsRequired();
			Property(p => p.EntityId).IsRequired();
			Property(p => p.EntityType).IsRequired();
		}
	}
}