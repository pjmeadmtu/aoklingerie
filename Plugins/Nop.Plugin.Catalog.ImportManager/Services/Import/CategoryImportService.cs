﻿using System;

using Nop.Core.Domain.Catalog;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Data;
using Nop.Plugin.Catalog.ImportManager.Domain.Services.Import;
using Nop.Services.Catalog;

namespace Nop.Plugin.Catalog.ImportManager.Services.Import
{
	public class CategoryImportService : BaseImportService<Category>, ICategoryImportService
	{
		public CategoryImportService(IExcelExtractService excelExtractService,
			ISourceMappingRefService sourceMappingRefService,
			ICategoryService categoryService)
			: base(excelExtractService,
				  sourceMappingRefService,
				  ImportEntityType.Category,
				  category => {
					  category.CreatedOnUtc = DateTime.UtcNow;
					  category.UpdatedOnUtc = DateTime.UtcNow;
					  categoryService.InsertCategory(category);
				  },
				  category => {
					  category.UpdatedOnUtc = DateTime.UtcNow;
					  categoryService.UpdateCategory(category);
				  }) { }
	}
}