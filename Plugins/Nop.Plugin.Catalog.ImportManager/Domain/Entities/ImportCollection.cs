﻿using System.Collections.Generic;
using Nop.Core;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class ImportCollection : BaseEntity
	{
		public string Name { get; set; }
		public virtual ICollection<ImportSource> ImportSources { get; set; }
	}
}