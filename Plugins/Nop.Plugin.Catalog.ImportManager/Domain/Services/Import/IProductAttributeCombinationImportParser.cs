﻿using System.Collections.Generic;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Services.Import
{
	public interface IProductAttributeCombinationImportParserService
	{
		string ParseAttributes(int productId, int sourceId, IEnumerable<int> entityIds);
	}
}