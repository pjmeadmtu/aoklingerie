﻿using Nop.Core;
using Nop.Plugin.Catalog.ImportManager.Domain.Enums;

namespace Nop.Plugin.Catalog.ImportManager.Domain.Entities
{
	public class SourceMappingRef : BaseEntity
	{
		public int SourceId { get; set; }
		public int EntityId { get; set; }
		public string SourceIdValue { get; set; }
		public ImportEntityType EntityType { get; set; }

		public virtual ImportSource ImportSource { get; set; }
	}
}